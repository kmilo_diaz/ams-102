#!/bin/bash

cd multiviews/server

/ams-102/paraview-git/bin/pvpython --force-offscreen-rendering ./pvw-light-viz.py  -p 1234 -c /ams-102/ams-102/multiviews/www/ --data /ams-102/data $*

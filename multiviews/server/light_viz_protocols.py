
import os, sys, logging, types, inspect, traceback, logging, re, json, base64
from time import time

# import RPC annotation
from wslink import register as exportRpc

# import paraview modules.
import paraview

from paraview import simple, servermanager
from paraview.web import protocols as pv_protocols

# Needed for:
#    vtkSMPVRepresentationProxy
#    vtkSMTransferFunctionProxy
#    vtkSMTransferFunctionManager
from vtk.vtkPVServerManagerRendering import vtkSMPVRepresentationProxy, vtkSMTransferFunctionProxy, vtkSMTransferFunctionManager

# Needed for:
#    vtkSMProxyManager
from vtk.vtkPVServerManagerCore import vtkSMProxyManager

# Needed for:
#    vtkDataObject
from vtk.vtkCommonDataModel import vtkDataObject


#import os, sys, re, json

# import paraview modules.
#from vtk.vtkPVServerManagerRendering import vtkSMPVRepresentationProxy, vtkSMTransferFunctionProxy, vtkSMTransferFunctionManager
#from paraview.web import protocols as pv_protocols
#from paraview import simple

# import RPC annotation
#from wslink import register as exportRpc
#from paraview import servermanager



# =============================================================================
#
# Viewport Size
#
# =============================================================================

# class LightVizViewportSize(pv_protocols.ParaViewWebProtocol):

#     # RpcName: mouseInteraction => viewport.mouse.interaction
#     @exportRpc("light.viz.viewport.size")
#     def updateSize(self, viewId, width, height):
#         view = self.getView(viewId)
#         view.ViewSize = [ width, height ]

# =============================================================================
#
# Configuration management
#
# =============================================================================

class LightVizConfig(pv_protocols.ParaViewWebProtocol):
    def __init__(self, config, defaultProfile):
        self.config = config
        self.defaultProfile = defaultProfile

    @exportRpc("light.viz.configuration.get")
    def getDefaultProfile(self):
        return [self.config, self.defaultProfile]

# =============================================================================

def simpleColorBy(rep=None, value=None):
    """Set scalar color. This will automatically setup the color maps and others
    necessary state for the representations. 'rep' must be the display
    properties proxy i.e. the value returned by GetDisplayProperties() function.
    If none is provided the display properties for the active source will be
    used, if possible."""
    rep = rep if rep else simple.GetDisplayProperties()
    if not rep:
        raise ValueError ("No display properties can be determined.")

    association = rep.ColorArrayName.GetAssociation()
    arrayname = rep.ColorArrayName.GetArrayName()
    component = None
    if value == None:
        rep.SetScalarColoring(None, servermanager.GetAssociationFromString(association))
        return
    if not isinstance(value, tuple) and not isinstance(value, list):
        value = (value,)
    if len(value) == 1:
        arrayname = value[0]
    elif len(value) >= 2:
        association = value[0]
        arrayname = value[1]
    if len(value) == 3:
        # component name provided
        componentName = value[2]
        if componentName == "Magnitude":
          component = -1
        else:
          if association == "POINTS":
            array = rep.Input.PointData.GetArray(arrayname)
          if association == "CELLS":
            array = rep.Input.CellData.GetArray(arrayname)
          if array:
            # looking for corresponding component name
            for i in range(0, array.GetNumberOfComponents()):
              if componentName == array.GetComponentName(i):
                component = i
                break
              # none have been found, try to use the name as an int
              if i ==  array.GetNumberOfComponents() - 1:
                try:
                  component = int(componentName)
                except ValueError:
                  pass
    if component is None:
      rep.SetScalarColoring(arrayname, servermanager.GetAssociationFromString(association))
    else:
      rep.SetScalarColoring(arrayname, servermanager.GetAssociationFromString(association), component)
    # rep.RescaleTransferFunctionToDataRange()




# =============================================================================
#
# View Set  -  This class represents a paraview render view. It has the data set
#              extracted from the files
# =============================================================================

class LightVizViews(pv_protocols.ParaViewWebProtocol):
   
    def __init__(self, viewid, viewName):
         self.name = viewName
         self.id = viewid
         self.datasetName = None
         self.dataset = None
         self.activeMeta = None
         self.extractBlocks = None
         self.datasetRep = None
         self.colormaps = {}
         self.reader = None
         self.foreground = [ 1, 1, 1]
         self.background = [ 0, 0, 0]
         self.background2 = [ 0, 0, 0]

# =============================================================================
#
# Dataset management - This class parses the file and create two LightVizViews instances
#                      representing each view in the front end app. Also is the backend for the data set UI TAB
# =============================================================================

class LightVizDatasets(pv_protocols.ParaViewWebProtocol):

    def __init__(self, data_directory, export_directory, viewsId, viewsNames):
        super(LightVizDatasets, self).__init__()
        self.basedir = data_directory
        self.exportdir = export_directory
        self.datasetMap = {}
        self.dataset = None
        self.reader = None
        self.context = None
        self.extractBlocks = None
        self.datasets = []
        self.activeMeta = None
        self.foreground = [ 1, 1, 1]
        self.background = [ 0, 0, 0]
        self.colorBy = [('__SOLID__', '__SOLID__'),('__SOLID__', '__SOLID__')]
        self.dataListeners = []
        self.initialBlocks = []
        self.geometryInitialBlocks = []


        dirname = os.path.dirname(__file__)

 
        view1 =  LightVizViews(viewsId[0],viewsNames[0])
        view2 =  LightVizViews(viewsId[1],viewsNames[1])
       
        self.views = [ view1,  view2 ] # array of rendered views. The views are retrived by their index.
        self.currentActiveViewId = None;

        #self.viewsId = viewsId
        #self.viewsNames = viewsNames

        self.cameraLink1 = 'mycameraLink1';
        self.cameraLink2 = 'mycameraLink2';

        #check the data dir is set, if not set one by default
        if not self.basedir:
          self.basedir = '/ams-102/light-viz/data/'

        print("data base dir =" + self.basedir)


        if not self.exportdir:
          self.exportdir = os.path.join(dirname, '../exports/');

        print("export base dir =" + self.exportdir)
        
        exportBasePath = os.path.abspath(self.exportdir)

        self.screenShotFolder  = os.path.join(exportBasePath, 'screenshots/') 
        self.modelsFolder  = os.path.join(exportBasePath, 'models/')
         
        print("screenshots base dir =" + self.screenShotFolder)
        print("model export base dir =" + self.modelsFolder)

        #self.view = simple.GetRenderView()

        for filePath in os.listdir(self.basedir):
            indexPath = os.path.join(self.basedir, filePath, 'index.json')
            if os.path.exists(indexPath):
                with open(indexPath, 'r') as fd:
                    metadata = json.loads(fd.read())
                    self.datasets.append(metadata)
                    self.datasetMap[metadata['name']] = { 'path':  os.path.dirname(indexPath), 'meta': metadata }


    def addListener(self, dataChangedInstance):
        self.dataListeners.append(dataChangedInstance)

    def getInput(self,viewIndex):
        return self.views[viewIndex].dataset

    # return a dictionary of numeric column names and their ranges, plus other initialization info.
    @exportRpc('my.protocols.views')
    def getViews(self):
       return [self.views[0].id,self.views[1].id]

    @exportRpc('my.protocols.active.view')
    def mySetActiveView(self,viewId):
      try:
       for view in self.views:
         if view.id == viewId:
          paraView = simple.FindView(viewId)
          simple.SetActiveView(paraView)
          self.currentActiveViewId = view.id
      except:
       print "Unexpected error my.protocols.active.view:", sys.exc_info()[0]


    @exportRpc('my.protocols.linkcameras')
    def linkCameras(self):
      #handle the case to synchronize view 1 to 2
      view1 = simple.FindView(self.views[0].name)
      view2 = simple.FindView(self.views[1].name)
      simple.AddCameraLink(view2,view1, self.cameraLink2);


    @exportRpc('my.protocols.unlinkcameras')
    def unLinkCameras(self):
      simple.RemoveCameraLink(self.cameraLink2);



    @exportRpc("light.viz.dataset.list")
    def listDatasets(self):
        self.datasets = []
        self.datasetMap = {}
        for filePath in os.listdir(self.basedir):
            indexPath = os.path.join(self.basedir, filePath, 'index.json')
            if os.path.exists(indexPath):
                with open(indexPath, 'r') as fd:
                    metadata = json.loads(fd.read())
                    self.datasets.append(metadata)
                    self.datasetMap[metadata['name']] = { 'path':  os.path.dirname(indexPath), 'meta': metadata }
        return self.datasets

    @exportRpc("light.viz.dataset.thumbnail")
    def getThumbnails(self, datasetName):
        thumbnails = []
        info = self.datasetMap[datasetName]
        oldVersion = servermanager.vtkSMProxyManager.GetVersionMinor() < 2 and servermanager.vtkSMProxyManager.GetVersionMajor() < 6
        if info:
            basePath = info['path']
            for fileName in info['meta']['thumbnails']:
                if oldVersion:
                  with open(os.path.join(basePath, fileName), 'rb') as image:
                    thumbnails.append('data:image/%s;base64,%s' % (fileName.split('.')[-1], base64.b64encode(image.read())))
                else:
                  thumbnails.append('/ds/%s/%s' % (datasetName, fileName))

        return thumbnails

    @exportRpc("light.viz.dataset.thumbnail.save")
    def saveThumbnail(self, viewIndex):
        view = simple.FindView(self.views[viewIndex].name)
        if view:
            size = [x for x in view.ViewSize.GetData()]
            view = [400, 400]
            basePath = self.datasetMap[self.views[viewIndex].activeMeta['name']]['path']
            numThumbnails = len(self.views[viewIndex].activeMeta['thumbnails'])
            filename = "thumbnail%d.jpg" % (numThumbnails + 1)
            filepath = os.path.join(basePath, filename)
            simple.SaveScreenshot(filepath,size)
            self.views[viewIndex].activeMeta['thumbnails'].append(filename)
            indexPath = os.path.join(self.basedir, basePath, 'index.json')
            with open(indexPath, 'w') as fd:
                fd.write(json.dumps(self.views[viewIndex].activeMeta, indent=4, separators=(',', ': ')))
            view = size

    @exportRpc("light.viz.dataset.screenshot.save")
    def saveScreenshot(self, viewIndex, ssFileName):
        view = simple.FindView(self.views[viewIndex].name)
        if view:
            #basePath = self.datasetMap[self.views[viewIndex].activeMeta['name']]['path']
            basePath = self.screenShotFolder
            filename = "%s.jpg" % (ssFileName)
            filepath = os.path.join(basePath, filename)
            simple.SaveScreenshot(filepath,view)
            return filepath


    @exportRpc("light.viz.dataset.model3d.save")
    def saveModel3D(self, viewIndex, modelFileName):
        #take screenshot first
        self.saveScreenshot(viewIndex,modelFileName)
        view = simple.FindView(self.views[viewIndex].name)
        if view:
            #basePath = self.datasetMap[self.views[viewIndex].activeMeta['name']]['path']
            basePath = self.modelsFolder
            filename = "%s.x3d" % (modelFileName)
            filepath = os.path.join(basePath, filename)
            simple.ExportView(filepath,view)
            return filepath


    #------------------------------------------------------------------------  
    # This is the main method that loads dataset form files. The viewIndex argument indicates the number of the view in the UI
    # Strongly recommended not to remove the prints calls. 
    #--------------------------------------------------------------------------
    @exportRpc("light.viz.dataset.load")
    def loadDataset(self, viewIndex, datasetName):
      print("setGeometryInView viewIndex: "+ str(viewIndex))
      print("setGeometryInView datasetName: "+ str(datasetName))
      #--------------------------  
      # Please dont uncommend the lines below. It helps on future debug events
      #--------------------------
      #cone =  simple.Cone();   
      #simple.Show(cone, view)
      #self.getApplication().InvokeEvent('UpdateEvent') # It doesnt update all the views at once. No idea why.
      #print(self.viewsNames)
     
      try:
        view = simple.FindView(self.views[viewIndex].name)
      except NameError: 
         traceback.print_exc()
      except:
        print "Unexpected error light.viz.dataset.load:", sys.exc_info()[0]
      

      if self.views[viewIndex].dataset:
         if self.views[viewIndex].activeMeta is self.datasetMap[datasetName]['meta']:
             return self.views[viewIndex].activeMeta
         simple.Delete(self.views[viewIndex].reader)
         if self.views[viewIndex].extractBlocks:
            simple.Delete(self.views[viewIndex].extractBlocks)
         self.views[viewIndex].dataset = None
         self.views[viewIndex].datasetRep = None

       
      if self.context:
        simple.Delete(self.views[viewIndex].reader)
        self.context = None
        self.contextRep = None
       
      print("setGeometryInView 1")
      self.views[viewIndex].activeMeta = self.datasetMap[datasetName]['meta']
       
      print("setGeometryInView 1.5")
      if 'context' in self.views[viewIndex].activeMeta['data']:
        print("setGeometryInView 1.5.1")
        self.context = simple.OpenDataFile(os.path.join(self.datasetMap[datasetName]['path'], self.views[viewIndex].activeMeta['data']['context']))
        self.contextRep = simple.Show(self.context,view)
      
      print("setGeometryInView 2")
      filesToLoad = []
      if type(self.views[viewIndex].activeMeta['data']['file']) is list:
          for fileName in self.views[viewIndex].activeMeta['data']['file']:
            filesToLoad.append(os.path.join(self.datasetMap[datasetName]['path'], fileName))
      else:
          filesToLoad.append(os.path.join(self.datasetMap[datasetName]['path'], self.views[viewIndex].activeMeta['data']['file']))

      print("setGeometryInView 3")
      self.views[viewIndex].reader = simple.OpenDataFile(filesToLoad)
      # Have to do this to force the reader to execute and get the data information
      readerRep = simple.Show(self.views[viewIndex].reader,view)
      readerRep.Visibility = 0
      
      print("setGeometryInView 3.1")
      if self.views[viewIndex].reader.GetDataInformation().DataInformation.GetCompositeDataInformation().GetDataIsComposite() == 1:
        self.views[viewIndex].extractBlocks = simple.ExtractBlock(Input = self.views[viewIndex].reader)
        print("setGeometryInView 3.1.1" )
        self.views[viewIndex].dataset = self.views[viewIndex].extractBlocks
        blocks = self.getBlockStructure(viewIndex) 
        while len(blocks[-1]['children']) > 0:
           blocks = blocks[-1]['children']        
        
        for block in blocks:        
          if  block['visibility'] == True:
            self.initialBlocks.append(block['flatindex'])

        blocksForGeometry = self.getBlockStructureForGeometry(viewIndex) 
        while len(blocksForGeometry[-1]['children']) > 0:
           blocksForGeometry = blocksForGeometry[-1]['children']        
        
        for geometryBlock in blocksForGeometry:        
          if  geometryBlock['visibility'] == True:
            self.geometryInitialBlocks.append(geometryBlock['flatindex'])


        #for x in range(blocks[-1]['flatindex']):
        #  print(x)
        #self.views[viewIndex].extractBlocks.BlockIndices = [ x + 1 for x in range(blocks[-1]['flatindex'])]
        self.views[viewIndex].extractBlocks.BlockIndices = self.initialBlocks

      else:
        print("setGeometryInView 3.1.2")
        self.views[viewIndex].dataset = self.views[viewIndex].reader

      print("setGeometryInView 3.2")
      self.views[viewIndex].datasetRep = simple.Show(self.dataset,view)
      print("setGeometryInView 3.2.1")
      self.views[viewIndex].datasetRep.Representation = 'Surface'
      self.views[viewIndex].datasetRep.Visibility = 1
      self.colorBy[viewIndex] = ('__SOLID__', '__SOLID__')
      print("setGeometryInView 3.2.2")
      self.updateColorBy(self.colorBy[viewIndex][1], self.colorBy[viewIndex][0],viewIndex)
      print("setGeometryInView 3.3")

      try:
        simple.Render(view)
        view.Background = self.background
      except AttributeError:
        traceback.print_exc()
      except:
        print "Unexpected error light.viz.dataset.load:", sys.exc_info()[0]

      # reset the camera
      print("setGeometryInView 4")
      simple.ResetCamera(view)
      view.CenterOfRotation = view.CameraFocalPoint
      simple.Render(view)

      self.getApplication().InvokeEvent('UpdateEvent')
      self.anim = simple.GetAnimationScene()

      # Notify listeners
      for l in self.dataListeners:
          try:
            l.dataChanged(viewIndex)
          except (TypeError,AttributeError):
            traceback.print_exc()
          except:
            print "Unexpected error light.viz.dataset.load:", sys.exc_info()[0]

      print("setGeometryInView 5")
      # When loading new dataset, rescale colormaps to full range by default
      for array in self.views[viewIndex].activeMeta['data']['arrays']:
         print("setGeometryInView 5.1")
         arrRange = array['range']
         arrName = array['name']
         print("setGeometryInView 5.2")
         self.setColormapRange(arrName, arrRange,viewIndex)

      print("setGeometryInView 6, view id: " + view.GetGlobalIDAsString())
      simple.Render(view)

      self.getApplication().InvokeEvent('UpdateEvent')
      
      self.views[viewIndex].datasetName = datasetName
      self.enableDataset(False,viewIndex)
      return self.views[viewIndex].activeMeta

    @exportRpc("light.viz.dataset.setblock.visibility")
    def setBlockVisibility(self, visible, viewIndex):
        # 0 block is always presumed to be needed since everything is under it
        try:
           if self.views[viewIndex].extractBlocks is None:
              return
           self.views[viewIndex].extractBlocks.BlockIndices = visible
           self.getApplication().InvokeEvent('UpdateEvent')
        except:
           print "Unexpected error light.viz.dataset.setblock.visibility:", sys.exc_info()[0]

        

    @exportRpc("light.viz.dataset.getblockstructure")
    def getBlockStructure(self,viewIndex):
        dataInfo = self.views[viewIndex].reader.GetDataInformation().DataInformation.GetCompositeDataInformation()
        if dataInfo.GetDataIsComposite() == 0:
            return []
        index = 0;
        #---------------------------
        def processInfo(info, index):
            output = []
            if info.GetNumberOfChildren() == 0:
                return output, index
            for i in xrange(info.GetNumberOfChildren()):
                name = info.GetName(i)
                visibility = True
                if "interior" in name:  
                  visibility = False
                childOutput = []
                index += 1
                myIndex = index
                if info.GetDataInformation(i) is not None:
                    childInfo = info.GetDataInformation(i).GetCompositeDataInformation()
                    childOutput, index = processInfo(childInfo, index)
                output.append({'name': name, 'children': childOutput, 'flatindex': myIndex, 'visibility':visibility})
            return output, index
        #---------------------------
        a, index = processInfo(dataInfo, index)
        return a

    def getBlockStructureForGeometry(self,viewIndex):
        dataInfo = self.views[viewIndex].reader.GetDataInformation().DataInformation.GetCompositeDataInformation()
        if dataInfo.GetDataIsComposite() == 0:
            return []
        index = 0;
        #---------------------------
        def processInfo(info, index):
            output = []
            if info.GetNumberOfChildren() == 0:
                return output, index
            for i in xrange(info.GetNumberOfChildren()):
                name = info.GetName(i)
                visibility = False
                if "wall" in name:  
                  visibility = True
                childOutput = []
                index += 1
                myIndex = index
                if info.GetDataInformation(i) is not None:
                    childInfo = info.GetDataInformation(i).GetCompositeDataInformation()
                    childOutput, index = processInfo(childInfo, index)
                output.append({'name': name, 'children': childOutput, 'flatindex': myIndex, 'visibility':visibility})
            return output, index
        #---------------------------
        a, index = processInfo(dataInfo, index)
        return a

    def checkArrayInMap(self, array, viewIndex):
        if array not in self.views[viewIndex].colormaps:
            self.views[viewIndex].colormaps[array] = { 'preset': 'Cool to Warm', 'range': [0, 1] }

    @exportRpc("light.viz.colormap.setpreset")
    def setColormapPreset(self, array, presetName, viewIndex):
        if (array is None):
            return
        self.checkArrayInMap(array,viewIndex)
        self.views[viewIndex].colormaps[array]['preset'] = presetName
        rtDataLUT = simple.GetColorTransferFunction(array);
        rtDataLUT.ApplyPreset(presetName, True)
        view = simple.FindView(self.views[viewIndex].name)
        simple.Render(view)
        self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.colormap.setrange")
    def setColormapRange(self, array, newRange, viewIndex):
        if (array is None):
            return
        self.checkArrayInMap(array,viewIndex)
        self.views[viewIndex].colormaps[array]['range'] = newRange
        rtDataLUT = simple.GetColorTransferFunction(array)
        rtDataLUT.RescaleTransferFunction(newRange[0], newRange[1])
        opacityLUT = simple.GetOpacityTransferFunction(array)
        opacityLUT.RescaleTransferFunction(newRange[0], newRange[1])
        self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.colormap.rescale.todatarange")
    def setColormapRangeToDataRange(self, array,viewIndex):
        if (array is None):
            return
        try:
          self.checkArrayInMap(array,viewIndex)
          rtDataLUT = simple.GetColorTransferFunction(array)
          self.views[viewIndex].datasetRep.RescaleTransferFunctionToDataRange(False)
          self.views[viewIndex].colormaps[array]['range'] = [rtDataLUT.RGBPoints[0], rtDataLUT.RGBPoints[-4]]
          rtDataOpacityTF = simple.GetOpacityTransferFunction(array)
          rtDataOpacityTF.RescaleTransferFunction(self.views[viewIndex].colormaps[array]['range'])

          self.getApplication().InvokeEvent('UpdateEvent')
                 
          return self.views[viewIndex].colormaps[array]['range']
        except (TypeError, AttributeError):
          traceback.print_exc()       
        except:
          print "Unexpected error light.viz.colormap.rescale.todatarange:", sys.exc_info()[0]

    @exportRpc("light.viz.colormap.get")
    def getColorMap(self, array, viewIndex):
        if (array is None):
            return { 'preset': 'Cool to Warm', 'range': [0, 1] }
        self.checkArrayInMap(array,viewIndex)
        return self.views[viewIndex].colormaps[array]

    @exportRpc("light.viz.opacitymap.set")
    def setOpacityMap(self, array, controlPoints):
        if (array is None):
            return
        points = []
        r = self.colormaps[array]['range']
        for p in controlPoints:
            points.append(p["x"] * (r[1] - r[0]) + r[0])
            points.append(p["y"])
            points.append(0.5)
            points.append(0.0)
        rtDataLUT = simple.GetOpacityTransferFunction(array);
        rtDataLUT.Points = points

        self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.opacitymap.get")
    def getOpacityMap(self, array, viewIndex):
        if (array is None):
            return
        points = []
        rtDataLUT = simple.GetOpacityTransferFunction(array);
        r = self.views[viewIndex].colormaps[array]['range']
        for i in xrange(len(rtDataLUT.Points) / 4):
            points.append((rtDataLUT.Points[i * 4] - r[0]) / (r[1] - r[0]))
            points.append(rtDataLUT.Points[i * 4 + 1])
        return points

    @exportRpc("light.viz.foreground.color")
    def setForegroundColor(self, foreground, viewIndex):
        try:
          self.views[viewIndex].foreground = [ float(x) for x in foreground.split(' ')]
          for obs in self.dataListeners:
             obs.setForegroundColor( self.views[viewIndex].foreground, viewIndex)
          if self.views[viewIndex].datasetRep:
             self.views[viewIndex].datasetRep.DiffuseColor = self.views[viewIndex].foreground
             self.getApplication().InvokeEvent('UpdateEvent')
        except AttributeError:
          traceback.print_exc()
        except:
          print "Unexpected error light.viz.foreground.color:", sys.exc_info()[0]

    @exportRpc("light.viz.background0.color")
    def setBackgroundColor(self, background, viewIndex):
        try:
         self.views[viewIndex].background = [ float(x) for x in background.split(' ')]
         view = simple.FindView(self.views[viewIndex].name)
         if view:
            view.Background = [0,0,0]
            view.Background2 = [0,0,0]
            self.getApplication().InvokeEvent('UpdateEvent')
        except AttributeError:
          traceback.print_exc()
        except:
         print "Unexpected error light.viz.background0.color:", sys.exc_info()[0]

    @exportRpc("light.viz.background1.color")
    def setBackgroundColor2(self, background, viewIndex):
        try:
         self.views[viewIndex].background = [ float(x) for x in background.split(' ')]
         view = simple.FindView(self.views[viewIndex].name)
         if view:
             view.Background = [0.3, 0.3, 0.3]
             view.Background2 = [0.3, 0.3, 0.3]   
             self.getApplication().InvokeEvent('UpdateEvent')
        except AttributeError:
          traceback.print_exc()
        except:
         print "Unexpected error light.viz.background1.color:", sys.exc_info()[0]

    @exportRpc("light.viz.background2.color")
    def setBackgroundColor3(self, background, viewIndex):
        try:
         self.views[viewIndex].background = [ float(x) for x in background.split(' ')]
         view = simple.FindView(self.views[viewIndex].name)
         if view:
            view.Background = [0.32,0.34,0.43]
            view.Background2 = [0,0,0.16]     
            self.getApplication().InvokeEvent('UpdateEvent')
        except AttributeError:
          traceback.print_exc()
        except:
         print "Unexpected error light.viz.background2.color:", sys.exc_info()[0]



    @exportRpc("light.viz.dataset.getstate")
    def getState(self,viewIndex):
        try:
          tmp = {
                  "opacity": self.views[viewIndex].datasetRep.Opacity,
                  "representation": self.views[viewIndex].datasetRep.Representation,
                  "color": self.colorBy[viewIndex],
                  "enabled": self.views[viewIndex].datasetRep.Visibility == 1,
                  #"enabled": False,
              }

          if not isinstance(tmp["enabled"], bool):
              tmp["enabled"] = tmp["enabled"][0]

          return tmp
        except AttributeError:
          traceback.print_exc()
        except:
          print "Unexpected error light.viz.dataset.getstate:", sys.exc_info()[0]

    @exportRpc("light.viz.dataset.opacity")
    def updateOpacity(self, opacity, viewIndex):
        try:
           if self.views[viewIndex].datasetRep:
              self.views[viewIndex].datasetRep.Opacity = opacity
              self.getApplication().InvokeEvent('UpdateEvent')
        except:
            print "Unexpected error light.viz.dataset.opacity:", sys.exc_info()[0]
        return opacity


    @exportRpc("light.viz.dataset.time")
    def updateTime(self, timeIdx):
        if len(self.anim.TimeKeeper.TimestepValues) > 0:
            self.anim.TimeKeeper.Time = self.anim.TimeKeeper.TimestepValues[timeIdx]
            self.getApplication().InvokeEvent('UpdateEvent')

        return self.anim.TimeKeeper.Time

    @exportRpc("light.viz.dataset.representation")
    def updateRepresentation(self, mode, viewIndex):
        try:
          if self.views[viewIndex].datasetRep:
               self.views[viewIndex].datasetRep.Representation = mode
               self.getApplication().InvokeEvent('UpdateEvent')
        except:
          print "Unexpected error light.viz.dataset.representation:", sys.exc_info()[0]

    @exportRpc("light.viz.dataset.color")
    def updateColorBy(self, field, location,viewIndex):
      currentView = simple.FindView(self.views[viewIndex].name)
      self.colorBy[viewIndex] = (location, field)
      if field == '__SOLID__':
          self.views[viewIndex].datasetRep.ColorArrayName = ''
      else:
          # Select data array
          simpleColorBy(self.views[viewIndex].datasetRep, (location, field))
          lutProxy = self.views[viewIndex].datasetRep.LookupTable
          pwfProxy = self.views[viewIndex].datasetRep.ScalarOpacityFunction
          for array in self.views[viewIndex].activeMeta['data']['arrays']:
              if array['name'] == field and array['location'] == location:
                 if lutProxy:
                     vtkSMTransferFunctionProxy.RescaleTransferFunction(lutProxy.SMProxy, array['range'][0], array['range'][1], False)
                 if pwfProxy:
                     vtkSMTransferFunctionProxy.RescaleTransferFunction(pwfProxy.SMProxy, array['range'][0], array['range'][1], False)
      simple.Render(currentView)
      self.getApplication().InvokeEvent('UpdateEvent')


    @exportRpc("light.viz.dataset.enable")
    def enableDataset(self, enable,viewIndex):
        try:
          self.views[viewIndex].datasetRep.Visibility = 1 if enable else 0
          currentView = simple.FindView(self.views[viewIndex].name)
          simple.Render(currentView)
          self.getApplication().InvokeEvent('UpdateEvent')
        except:
          print "Unexpected error light.viz.dataset.enable:", sys.exc_info()[0]


# =============================================================================
#
# Geometry management
#
# =============================================================================

class LightVizGeometry(pv_protocols.ParaViewWebProtocol):

    def __init__(self, dataset_manager):
        super(LightVizGeometry, self).__init__()
        self.ds = dataset_manager
        self.opacity = [.5,.5]
        self.representation = [None, None]
        self.extractBlock = [None, None]
        self.reprMode = ['Surface','Surface']
        self.colorBy = [('__SOLID__', '__SOLID__'),('__SOLID__', '__SOLID__')]
        dataset_manager.addListener(self)


    def dataChanged(self,viewIndex):
        self.updateRepresentation('Surface', viewIndex)
        self.updateColorBy('__SOLID__', '__SOLID__', viewIndex)
        self.enableGeometry(True, viewIndex)
        if self.extractBlock[viewIndex]:
          self.representation[viewIndex].DiffuseColor = [0.5, 0.5, 0.5]
          self.representation[viewIndex].Opacity = self.opacity[viewIndex]

    def setForegroundColor(self, foreground,viewIndex):
        if self.representation[viewIndex]:
            self.representation[viewIndex].DiffuseColor = foreground

    @exportRpc("light.viz.geometry.getstate")
    def getState(self, viewIndex):
        tmp = {
                "opacity": self.opacity[viewIndex],
                "color": self.colorBy[viewIndex],
                "enabled": True,
            }

        if not isinstance(tmp["enabled"], bool):
            tmp["enabled"] = tmp["enabled"][0]

        return tmp

    @exportRpc("light.viz.geometry.opacity")
    def updateOpacity(self, opacity,viewIndex):
        if self.representation[viewIndex]:
            self.representation[viewIndex].Opacity = opacity
            self.getApplication().InvokeEvent('UpdateEvent')

        return opacity

    @exportRpc("light.viz.geometry.representation")
    def updateRepresentation(self, mode,viewIndex):
        self.reprMode[viewIndex] = mode
        if self.representation[viewIndex]:
            self.representation[viewIndex].Representation = mode
            self.getApplication().InvokeEvent('UpdateEvent')
          
    @exportRpc("light.viz.geometry.color")
    def updateColorBy(self, field, location,viewIndex):
        self.colorBy[viewIndex] = (location, field)
        if self.representation[viewIndex]:
            if field == '__SOLID__':
                self.representation[viewIndex].ColorArrayName = ''
                self.representation[viewIndex].DiffuseColor = [0.5, 0.5, 0.5]
            else:
                simpleColorBy(self.representation[viewIndex], self.colorBy[viewIndex])
                # Update data array range
                lutProxy = self.representation[viewIndex].LookupTable
                for array in self.ds.views[viewIndex].activeMeta['data']['arrays']:
                    if array['name'] == field and array['location'] == location:
                        vtkSMTransferFunctionProxy.RescaleTransferFunction(lutProxy.SMProxy, array['range'][0], array['range'][1], False)
            view = simple.FindView(self.ds.views[viewIndex].name)
            simple.Render(view)
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.geometry.enable")
    def enableGeometry(self, enable, viewIndex):
        print("enableGeometry 1 index: " + str(viewIndex))
        if enable and self.ds.getInput(viewIndex):
          print("enableGeometry 2")
          if not self.extractBlock[viewIndex]:
            print("enableGeometry 3")
            self.extractBlock[viewIndex] = simple.ExtractBlock(Input=self.ds.getInput(viewIndex))

            # Properties modified on extractBlock1
            self.extractBlock[viewIndex].BlockIndices = self.ds.geometryInitialBlocks

          else:
              self.extractBlock[viewIndex].Input = self.ds.getInput(viewIndex)

          if not self.representation[viewIndex]:
              print("enableGeometry 4")
              view = simple.FindView(self.ds.views[viewIndex].name)
              self.representation[viewIndex] = simple.Show(self.extractBlock[viewIndex],view)
              self.representation[viewIndex].Representation = self.reprMode[viewIndex]
              self.representation[viewIndex].DiffuseColor = self.ds.foreground
              self.updateColorBy(self.colorBy[viewIndex][1], self.colorBy[viewIndex][0],viewIndex)
              print("enableGeometry 4.5")
              self.representation[viewIndex].Opacity = self.opacity[viewIndex]
              self.representation[viewIndex].DiffuseColor = [0.5, 0.5, 0.5]
              print("enableGeometry 5")
 
          self.representation[viewIndex].Visibility = 1

        if not enable and self.representation[viewIndex]:
            self.representation[viewIndex].Visibility = 0
        
        view = simple.FindView(self.ds.views[viewIndex].name)
        simple.Render(view)
        self.getApplication().InvokeEvent('UpdateEvent')
        print("enableGeometry 5")


# =============================================================================
#
# Clip management
#
# =============================================================================

class LightVizClip(pv_protocols.ParaViewWebProtocol):

    def __init__(self, dataset_manager):
        super(LightVizClip, self).__init__()
        self.ds = dataset_manager
        self.clipX = [None,None]
        self.clipY = [None, None]
        self.clipZ = [None, None]
        self.representation = [None,None]
        self.box = [None, None]
        self.boxRepr = [None, None]
        self.reprMode = ['Surface','Surface']
        self.colorBy = [('__SOLID__', '__SOLID__'), ('__SOLID__', '__SOLID__')]
        dataset_manager.addListener(self)

    def dataChanged(self,viewIndex):
        self.updateRepresentation('Surface', viewIndex)
        self.updateColorBy('__SOLID__', '__SOLID__',viewIndex)
        if self.clipX[viewIndex]:
            self.clipX[viewIndex].Input = self.ds.getInput(viewIndex)
            bounds = self.ds.views[viewIndex].activeMeta['data']['bounds']
            self.updatePosition(viewIndex,(bounds[1] + bounds[0])/2.0,
                                (bounds[3] + bounds[2])/2.0,
                                (bounds[5] + bounds[4])/2.0)
            self.updateInsideOut(viewIndex,False, False, False)
        if self.representation[viewIndex]:
            self.representation.Visibility = 0

    def setForegroundColor(self, foreground,viewIndex):
        if self.representation[viewIndex]:
            self.representation[viewIndex].DiffuseColor = foreground

    @exportRpc("light.viz.clip.box.position")
    def updatePositionForBox(self,viewIndex, x, y, z):
        newClipCenter = [x, y, z]
        boundsPoint = [self.ds.views[viewIndex].activeMeta['data']['bounds'][2 * i] for i in range(3)]
        if not self.clipX[viewIndex].InsideOut:
            boundsPoint[0] = self.ds.views[viewIndex].activeMeta['data']['bounds'][1]
        if not self.clipY[viewIndex].InsideOut:
            boundsPoint[1] = self.ds.views[viewIndex].activeMeta['data']['bounds'][3]
        if not self.clipZ[viewIndex].InsideOut:
            boundsPoint[2] = self.ds.views[viewIndex].activeMeta['data']['bounds'][5]
        if self.box:
            self.box[viewIndex].Center = [(newClipCenter[i] + boundsPoint[i]) * 0.5 for i in range(3)]
            self.box[viewIndex].XLength = abs(boundsPoint[0] - newClipCenter[0])
            self.box[viewIndex].YLength = abs(boundsPoint[1] - newClipCenter[1])
            self.box[viewIndex].ZLength = abs(boundsPoint[2] - newClipCenter[2])

        self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.clip.box.show")
    def showBox(self, enable,viewIndex):
        if enable and self.ds.getInput(viewIndex):
            bounds = self.ds.views[viewIndex].activeMeta['data']['bounds']
            if not self.box[viewIndex]:
                self.box[viewIndex] = simple.Box()
                self.updatePositionForBox(viewIndex,(bounds[1] + bounds[0])/2.0,
                                          (bounds[3] + bounds[2])/2.0,
                                          (bounds[5] + bounds[4])/2.0)
                view = simple.FindView(self.ds.views[viewIndex].name)
                self.boxRepr[viewIndex] = simple.Show(self.box[viewIndex],view)
                self.boxRepr[viewIndex].Representation = 'Outline'
                self.boxRepr[viewIndex].DiffuseColor = [1, 0, 0]
            self.boxRepr[viewIndex].Visibility = 1
        elif (not enable) and self.boxRepr[viewIndex]:
            self.boxRepr[viewIndex].Visibility = 0

        self.getApplication().InvokeEvent('UpdateEvent')


    @exportRpc("light.viz.clip.getstate")
    def getState(self,viewIndex):
        try:
          ret = {
              "representation": self.reprMode[viewIndex],
              "color": self.colorBy[viewIndex],
              "enabled": False,
              "xPosition": 0,
              "yPosition": 0,
              "zPosition": 0,
              "xInsideOut": False,
              "yInsideOut": False,
              "zInsideOut": False,
          }
          if self.representation[viewIndex]:
              ret["enabled"] = self.representation[viewIndex].Visibility == 1,
          if self.clipX[viewIndex]:
              ret["xPosition"] = self.clipX[viewIndex].ClipType.Origin[0]
              ret["yPosition"] = self.clipY[viewIndex].ClipType.Origin[1]
              ret["zPosition"] = self.clipZ[viewIndex].ClipType.Origin[2]
              ret["xInsideOut"] = self.clipX[viewIndex].InsideOut == 1
              ret["yInsideOut"] = self.clipY[viewIndex].InsideOut == 1
              ret["zInsideOut"] = self.clipZ[viewIndex].InsideOut == 1

          if not isinstance(ret["enabled"], bool):
              ret["enabled"] = ret["enabled"][0]

          return ret
        except AttributeError:
          traceback.print_exc()
        except:
          print "Unexpected error light.viz.clip.enable:", sys.exc_info()[0]

    @exportRpc("light.viz.clip.position")
    def updatePosition(self,viewIndex, x, y, z):
        if self.clipX[viewIndex]:
            self.clipX[viewIndex].ClipType.Origin = [float(x), 0.0, 0.0]
        if self.clipY[viewIndex]:
            self.clipY[viewIndex].ClipType.Origin = [0.0, float(y), 0.0]
        if self.clipZ[viewIndex]:
            self.clipZ[viewIndex].ClipType.Origin = [0.0, 0.0, float(z)]

        self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.clip.insideout")
    def updateInsideOut(self, viewIndex,x, y, z):
        if self.clipX[viewIndex]:
            self.clipX.InsideOut = 1 if x else 0
        if self.clipY[viewIndex]:
            self.clipY.InsideOut = 1 if y else 0
        if self.clipZ[viewIndex]:
            self.clipZ.InsideOut = 1 if z else 0

        self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.clip.representation")
    def updateRepresentation(self, mode, viewIndex):
        self.reprMode[viewIndex] = mode
        if self.representation[viewIndex]:
            self.representation[viewIndex].Representation = mode
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.clip.color")
    def updateColorBy(self, field, location, viewIndex):

        self.colorBy[viewIndex] = (location, field)
        if self.representation[viewIndex]:
           if field == '__SOLID__':
              self.representation[viewIndex].ColorArrayName = ''
           else:
              simpleColorBy(self.representation[viewIndex], (location, field))
              lutProxy = self.representation[viewIndex].LookupTable
              pwfProxy = self.representation[viewIndex].ScalarOpacityFunction
              for array in self.ds.views[viewIndex].activeMeta['data']['arrays']:
                  if array['name'] == field and array['location'] == location:
                     vtkSMTransferFunctionProxy.RescaleTransferFunction(lutProxy.SMProxy, array['range'][0], array['range'][1], False)
                     if pwfProxy:
                       vtkSMTransferFunctionProxy.RescaleTransferFunction(pwfProxy.SMProxy, array['range'][0], array['range'][1], False)

           currentView = simple.FindView(self.ds.views[viewIndex].name)
           simple.Render(currentView)
           self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.clip.enable")
    def enableClip(self, enable,viewIndex):
       try:
          if enable and self.ds.getInput(viewIndex):
              if not self.clipX[viewIndex]:
                  bounds = self.ds.views[viewIndex].activeMeta['data']['bounds']
                  center = [(bounds[i*2] + bounds[i*2+1])*0.5 for i in range(3)]
                  self.clipX[viewIndex] = simple.Clip(Input=self.ds.getInput(viewIndex))
                  self.clipY[viewIndex] = simple.Clip(Input=self.clipX)
                  self.clipZ[viewIndex] = simple.Clip(Input=self.clipY)
                                       
                  self.clipX[viewIndex].ClipType.Origin = center
                  self.clipX[viewIndex].ClipType.Normal = [1, 0, 0]
                  self.clipY[viewIndex].ClipType.Origin = center
                  self.clipY[viewIndex].ClipType.Normal = [0, 1, 0]
                  self.clipZ[viewIndex].ClipType.Origin = center
                  self.clipZ[viewIndex].ClipType.Normal = [0, 0, 1]
              else:
                  self.clipX[viewIndex].Input = self.ds.getInput()
                           
              if not self.representation[viewIndex]:
                  self.representation[viewIndex] = simple.Show(self.clipZ[viewIndex])
                  self.representation[viewIndex].Representation = self.reprMode[viewIndex]
                  self.representation[viewIndex].DiffuseColor = self.ds.foreground
                  self.updateColorBy(self.colorBy[viewIndex][1], self.colorBy[viewIndex][0],viewIndex)

              self.representation[viewIndex].Visibility = 1

          if not enable and self.representation[viewIndex]:
              self.representation[viewIndex].Visibility = 0
          
          currentView = simple.FindView(self.ds.views[viewIndex].name)                                                
          simple.Render(currentView)
          self.getApplication().InvokeEvent('UpdateEvent')
       except:
          print "Unexpected error light.viz.clip.enable:", sys.exc_info()[0]
                
    def getOutput(self,viewIndex):
        if not self.clipX[viewIndex]:
            bounds = self.ds.views[viewIndex].activeMeta['data']['bounds']
            center = [(bounds[i*2] + bounds[i*2+1])*0.5 for i in range(3)]
            
            view = simple.FindView(self.ds.views[viewIndex].name)
            self.clipX[viewIndex] = simple.Clip(view, Input=self.ds.getInput(viewIndex))
            self.clipY[viewIndex] = simple.Clip(view, Input=self.clipX[viewIndex])
            self.clipZ[viewIndex] = simple.Clip(view, Input=self.clipY[viewIndex])

            self.clipX[viewIndex].ClipType.Origin = center
            self.clipX[viewIndex].ClipType.Normal = [1, 0, 0]
            self.clipY[viewIndex].ClipType.Origin = center
            self.clipY[viewIndex].ClipType.Normal = [0, 1, 0]
            self.clipZ[viewIndex].ClipType.Origin = center
            self.clipZ[viewIndex].ClipType.Normal = [0, 0, 1]

        return self.clipZ[viewIndex]



# =============================================================================
#
# Contours management
#
# =============================================================================

class LightVizContour(pv_protocols.ParaViewWebProtocol):

    def __init__(self, dataset_manager, clip):
        super(LightVizContour, self).__init__()
        self.ds = dataset_manager
        self.clip = clip
        self.contour = [None,None]
        self.contourByField = [None, None]
        self.representation = [None,None]
        self.reprMode = ['Surface','Surface']
        self.colorBy = [('__SOLID__', '__SOLID__'),('__SOLID__', '__SOLID__')]
        self.useClippedInput = [False,False]
        dataset_manager.addListener(self)

    def dataChanged(self,viewIndex):
        self.updateRepresentation('Surface',viewIndex)
        self.updateColorBy(self.ds.views[viewIndex].activeMeta["data"]["arrays"][0]["name"], self.ds.views[viewIndex].activeMeta["data"]["arrays"][0]["location"],viewIndex)
        if self.contour[viewIndex]:
            self.contour[viewIndex].Input = self.ds.getInput(viewIndex)
            self.contour[viewIndex].Isosurfaces = [ sum(self.ds.views[viewIndex].activeMeta["data"]["arrays"][0]["range"]) * 0.5, ]
            self.representation[viewIndex].Visibility = 0

    def setForegroundColor(self, foreground,viewIndex):
        if self.representation[viewIndex]:
            self.representation[viewIndex].DiffuseColor = foreground
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.contour.useclipped")
    def setUseClipped(self, useClipped,viewIndex):
        if self.contour[viewIndex]:
            if not self.useClippedInput[viewIndex] and useClipped:
                self.contour[viewIndex].Input = self.clip.getOutput(viewIndex)
            elif self.useClippedInput[viewIndex] and not useClipped:
                self.contour[viewIndex].Input = self.ds.getInput(viewIndex)
        self.useClippedInput[viewIndex] = useClipped
        self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.contour.getstate")
    def getState(self,viewIndex):
        ret = {
            "representation": "Surface",
            "color": self.colorBy[viewIndex],
            "enabled": False,
            "field": '',
            "use_clipped": self.useClippedInput[viewIndex],
            "values": [],
        }
        if self.contour[viewIndex]:
            ret["representation"] = self.representation[viewIndex].Representation
            ret["color"] = self.colorBy[viewIndex]
            ret["enabled"] = self.representation[viewIndex].Visibility == 1,
            ret["field"] = self.contour[viewIndex].ContourBy[1]
            ret["values"] = [i for i in self.contour[viewIndex].Isosurfaces]


        if not isinstance(ret["enabled"], bool):
            ret["enabled"] = ret["enabled"][0]

        return ret

    @exportRpc("light.viz.contour.values")
    def updateValues(self, values, viewIndex):
        print("contour updateValues 1")
        try:
          if self.contour[viewIndex]:
             self.contour[viewIndex].Isosurfaces = values
             self.getApplication().InvokeEvent('UpdateEvent')
          print("contour updateValues 2")
        except (TypeError,NameError):
          traceback.print_exc()
        except:
          print "Unexpected error light.viz.contour.values:", sys.exc_info()[0]

    @exportRpc("light.viz.contour.by")
    def updateContourBy(self, field, viewIndex):
        print("updateContourBy 0 field: " + field)
        try:
          if self.contour[viewIndex]:
             print("updateContourBy 1")
             self.contourByField[viewIndex] = None
             self.contour[viewIndex].ContourBy = field
             self.getApplication().InvokeEvent('UpdateEvent')
          else:
             print("updateContourBy 2")
             self.contourByField[viewIndex] = field
          print("updateContourBy 3")
        except:
          print "Unexpected error light.viz.contour.by:", sys.exc_info()[0]

    @exportRpc("light.viz.contour.representation")
    def updateRepresentation(self, mode, viewIndex):
        self.reprMode[viewIndex] = mode
        if self.representation[viewIndex]:
            self.representation[viewIndex].Representation = mode
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.contour.color")
    def updateColorBy(self, field, location,viewIndex):
        self.colorBy[viewIndex] = (location, field)
        if self.representation[viewIndex]:
            if field == '__SOLID__':
                self.representation[viewIndex].ColorArrayName = ''
            else:
                simpleColorBy(self.representation[viewIndex], (location, field))
                lutProxy = self.representation[viewIndex].LookupTable
                for array in self.ds.views[viewIndex].activeMeta['data']['arrays']:
                    if array['name'] == field and array['location'] == location:
                        vtkSMTransferFunctionProxy.RescaleTransferFunction(lutProxy.SMProxy, array['range'][0], array['range'][1], False)
            view = simple.FindView(self.ds.views[viewIndex].name)
            simple.Render(view)
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.contour.enable")
    def enableContour(self, enable,viewIndex):
        print("enableContour 1 index: " + str(viewIndex))
        if enable and self.ds.getInput(viewIndex):
            print("enableContour 2")
            inpt = self.ds.getInput(viewIndex) if not self.useClippedInput[viewIndex] else self.clip.getOutput(viewIndex)
            if not self.contour[viewIndex]:
                print("enableContour 3.1")
                view = simple.FindView(self.ds.views[viewIndex].name)
                self.contour[viewIndex] = simple.Contour(Input=inpt, ComputeScalars=1, ComputeNormals=1)
                print("enableContour 3.1.1")
                self.representation[viewIndex] = simple.Show(self.contour[viewIndex],view)
                print("enableContour 3.1.2")
                self.representation[viewIndex].Representation = self.reprMode[viewIndex]
                self.representation[viewIndex].DiffuseColor = self.ds.foreground
                self.updateColorBy(self.colorBy[viewIndex][1], self.colorBy[viewIndex][0],viewIndex)
                print("enableContour 3.2")
                if self.contourByField[viewIndex]:
                  self.updateContourBy(self.contourByField[viewIndex],viewIndex)
            else:
                print("enableContour 4")
                self.contour[viewIndex].Input = inpt

            self.representation[viewIndex].Visibility = 1

        if not enable and self.representation[viewIndex]:
            self.representation[viewIndex].Visibility = 0

        view = simple.FindView(self.ds.views[viewIndex].name)
        simple.Render(view)
        self.getApplication().InvokeEvent('UpdateEvent')
        print("enableContour 5")

# =============================================================================
#
# Slice management
#
# =============================================================================

class LightVizSlice(pv_protocols.ParaViewWebProtocol):

    def __init__(self, dataset_manager, clip):
        super(LightVizSlice, self).__init__()
        self.ds = dataset_manager
        self.clip = clip
        self.sliceX = None
        self.sliceY = None
        self.sliceZ = None
        self.representationX = None
        self.representationY = None
        self.representationZ = None
        self.center = None
        self.visible = [1, 1, 1]
        self.enabled = False
        self.reprMode = 'Surface'
        self.colorBy = ('__SOLID__', '__SOLID__')
        self.useClippedInput = False
        #dataset_manager.addListener(self)

    def dataChanged(self,viewIndex):
        self.updateRepresentation('Surface')
        self.updateColorBy('__SOLID__', '__SOLID__')
        if self.sliceX:
            bounds = self.ds.activeMeta['data']['bounds']
            center = [(bounds[i*2] + bounds[i*2+1])*0.5 for i in range(3)]
            self.sliceX.Input = self.ds.getInput()
            self.sliceY.Input = self.ds.getInput()
            self.sliceZ.Input = self.ds.getInput()
            self.updatePosition(center[0], center[1], center[2])
            self.representationX.Representation = 'Surface'
            self.representationY.Representation = 'Surface'
            self.representationZ.Representation = 'Surface'
            self.representationX.Visibility = 0
            self.representationY.Visibility = 0
            self.representationZ.Visibility = 0
            self.enabled = False

    def setForegroundColor(self, foreground):
        if self.representationX:
            self.representationX.DiffuseColor = foreground
            self.representationY.DiffuseColor = foreground
            self.representationZ.DiffuseColor = foreground

    @exportRpc("light.viz.slice.useclipped")
    def setUseClipped(self, useClipped):
        if self.sliceX:
            if not self.useClippedInput and useClipped:
                for slice in [self.sliceX, self.sliceY, self.sliceZ]:
                    slice.Input = self.clip.getOutput()
            elif self.useClippedInput and not useClipped:
                for slice in [self.sliceX, self.sliceY, self.sliceZ]:
                    slice.Input = self.ds.getInput()
        self.useClippedInput = useClipped
        self.getApplication().InvokeEvent('UpdateEvent')


    @exportRpc("light.viz.slice.getstate")
    def getState(self):
        ret = {
            "representation": self.reprMode,
            "color": self.colorBy,
            "enabled": self.enabled,
            "xPosition": 0,
            "yPosition": 0,
            "zPosition": 0,
            "xVisible": self.visible[0] == 1,
            "yVisible": self.visible[1] == 1,
            "zVisible": self.visible[2] == 1,
            "use_clipped": self.useClippedInput,
        }
        if self.center:
            ret['xPosition'] = self.center[0]
            ret['yPosition'] = self.center[1]
            ret['zPosition'] = self.center[2]

        if not isinstance(ret["enabled"], bool):
            ret["enabled"] = ret["enabled"][0]

        return ret

    @exportRpc("light.viz.slice.position")
    def updatePosition(self, x, y, z):
        self.center = [x, y, z]
        if self.sliceX:
            self.sliceX.SliceType.Origin = self.center
        if self.sliceY:
            self.sliceY.SliceType.Origin = self.center
        if self.sliceZ:
            self.sliceZ.SliceType.Origin = self.center
        self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.slice.visibility")
    def updateVisibility(self, x, y, z):
        self.visible = [ 1 if x else 0, 1 if y else 0, 1 if z else 0]
        if self.representationX:
            self.representationX.Visibility = self.visible[0] and self.enabled
        if self.representationY:
            self.representationY.Visibility = self.visible[1] and self.enabled
        if self.representationZ:
            self.representationZ.Visibility = self.visible[2] and self.enabled
        self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.slice.representation")
    def updateRepresentation(self, mode):
        self.reprMode = mode
        if self.representationX:
            self.representationX.Representation = mode
            self.representationY.Representation = mode
            self.representationZ.Representation = mode
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.slice.color")
    def updateColorBy(self, field, location):
        self.colorBy = (location, field)
        if self.representationX:
            if field == '__SOLID__':
                self.representationX.ColorArrayName = ''
                self.representationY.ColorArrayName = ''
                self.representationZ.ColorArrayName = ''
            else:
                simpleColorBy(self.representationX, self.colorBy)
                simpleColorBy(self.representationY, self.colorBy)
                simpleColorBy(self.representationZ, self.colorBy)
                # Update data array range
                for rep in [self.representationX, self.representationY, self.representationZ]:
                  lutProxy = rep.LookupTable
                  for array in self.ds.activeMeta['data']['arrays']:
                      if array['name'] == field and array['location'] == location:
                          vtkSMTransferFunctionProxy.RescaleTransferFunction(lutProxy.SMProxy, array['range'][0], array['range'][1], False)

            simple.Render()
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.slice.enable")
    def enableSlice(self, enable,viewIndex):
        if enable and self.ds.getInput():
            inpt = self.ds.getInput() if not self.useClippedInput else self.clip.getOutput()
            if not self.sliceX:
                bounds = self.ds.activeMeta['data']['bounds']
                center = self.center
                if center is None:
                    center = [(bounds[i*2] + bounds[i*2+1])*0.5 for i in range(3)]
                self.sliceX = simple.Slice(Input=inpt)
                self.sliceY = simple.Slice(Input=inpt)
                self.sliceZ = simple.Slice(Input=inpt)

                self.sliceX.SliceType.Origin = center
                self.sliceX.SliceType.Normal = [1, 0, 0]
                self.sliceY.SliceType.Origin = center
                self.sliceY.SliceType.Normal = [0, 1, 0]
                self.sliceZ.SliceType.Origin = center
                self.sliceZ.SliceType.Normal = [0, 0, 1]

                self.representationX = simple.Show(self.sliceX)
                self.representationY = simple.Show(self.sliceY)
                self.representationZ = simple.Show(self.sliceZ)

                self.representationX.DiffuseColor = self.ds.foreground
                self.representationY.DiffuseColor = self.ds.foreground
                self.representationZ.DiffuseColor = self.ds.foreground

                self.updateRepresentation(self.reprMode)
                self.updateColorBy(self.colorBy[1], self.colorBy[0])
            else:
                self.sliceX.Input = inpt
                self.sliceY.Input = inpt
                self.sliceZ.Input = inpt
            self.representationX.Visibility = self.visible[0]
            self.representationY.Visibility = self.visible[1]
            self.representationZ.Visibility = self.visible[2]

        if not enable and self.representationX:
            self.representationX.Visibility = 0
            self.representationY.Visibility = 0
            self.representationZ.Visibility = 0

        self.enabled = enable
        simple.Render()
        self.getApplication().InvokeEvent('UpdateEvent')

# =============================================================================
#
# Multi-Slice management
#
# =============================================================================

class LightVizMultiSlice(pv_protocols.ParaViewWebProtocol):

    def __init__(self, dataset_manager, clip):
        super(LightVizMultiSlice, self).__init__()
        self.ds = dataset_manager
        self.clip = clip
        self.slice = [None, None]
        self.representation = [None, None]
        self.normal = [0,0]
        self.slicePositions = [[],[]]
        self.reprMode = ["Surface","Surface"]
        self.colorBy = [('__SOLID__', '__SOLID__'),('__SOLID__', '__SOLID__')]
        self.useClippedInput = [False,False]
        dataset_manager.addListener(self)

    def dataChanged(self,viewIndex):
        self.updateRepresentation('Surface', viewIndex)
        self.updateColorBy(self.ds.views[viewIndex].activeMeta["data"]["arrays"][0]["name"], self.ds.views[viewIndex].activeMeta["data"]["arrays"][0]["location"], viewIndex)
        bounds = self.ds.views[viewIndex].activeMeta['data']['bounds']
        center = [(bounds[i*2] + bounds[i*2+1])*0.5 for i in range(3)]
        self.updateNormal(0,viewIndex)
        self.updateSlicePositions([center[0]], viewIndex)
        if self.slice[viewIndex]:
            self.slice[viewIndex].Input = self.ds.views[viewIndex].getInput()
            self.representation[viewIndex].Visibility = 0

    def setForegroundColor(self, foreground, viewIndex):
        if self.representation[viewIndex]:
            self.representation[viewIndex].DiffuseColor = foreground

    @exportRpc("light.viz.mslice.useclipped")
    def setUseClipped(self, useClipped, viewIndex):
        if self.slice[viewIndex]:
            if not self.useClippedInput[viewIndex] and useClipped:
                self.slice[viewIndex].Input = self.clip.getOutput(viewIndex)
            elif self.useClippedInput[viewIndex] and not useClipped:
                self.slice[viewIndex].Input = self.ds.getInput(viewIndex)
        self.useClippedInput[viewIndex] = useClipped
        self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.mslice.getstate")
    def getState(self, viewIndex):
        ret = {
            'enabled': False,
            'representation': self.reprMode[viewIndex],
            'color': self.colorBy[viewIndex],
            'positions': self.slicePositions[viewIndex],
            'normal': str(self.normal[viewIndex]),
            "use_clipped": self.useClippedInput[viewIndex],
        }
        if self.representation[viewIndex]:
            ret["enabled"] = True if self.representation[viewIndex].Visibility else False
        return ret


    @exportRpc("light.viz.mslice.normal")
    def updateNormal(self, normalAxis,viewIndex):
       try:
        print("updateNormal 1 index: "+ str(viewIndex))
        self.normal[viewIndex] = int(normalAxis)
        if self.slice[viewIndex]:
            print("updateNormal 1.2")
            normal = [0, 0, 0]
            normal[self.normal[viewIndex]] = 1
            print("updateNormal 1.3")
            self.slice[viewIndex].SliceType.Normal = normal
            print("updateNormal 1.4")
            self.getApplication().InvokeEvent('UpdateEvent')
            print("updateNormal 1.5")
        print("updateNormal 2")
       except:
          print "Unexpected error light.viz.mslice.normal:", sys.exc_info()[0]

    @exportRpc("light.viz.mslice.positions")
    def updateSlicePositions(self, positions, viewIndex):
        print("updateSlicePositions 1 index: "+ str(viewIndex))
        self.slicePositions[viewIndex] = positions;
        print("updateSlicePositions 2")
        if self.slice[viewIndex]:
            print("updateSlicePositions 3")
            self.slice[viewIndex].SliceOffsetValues = positions
            print("updateSlicePositions 4")
            self.getApplication().InvokeEvent('UpdateEvent')
        print("updateSlicePositions 5")

    @exportRpc("light.viz.mslice.representation")
    def updateRepresentation(self, mode, viewIndex):
        self.reprMode[viewIndex] = mode
        if self.representation[viewIndex]:
            self.representation[viewIndex].Representation = mode
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.mslice.color")
    def updateColorBy(self, field, location, viewIndex):
        print("mslice updateColorBy " + str(viewIndex))
        self.colorBy[viewIndex] = (location, field)
        if self.representation[viewIndex]:
            if field == '__SOLID__':
                self.representation[viewIndex].ColorArrayName = ''
            else:
                simpleColorBy(self.representation[viewIndex], self.colorBy[viewIndex])
                # Update data array range
                lutProxy = self.representation[viewIndex].LookupTable
                for array in self.ds.views[viewIndex].activeMeta['data']['arrays']:
                    if array['name'] == field and array['location'] == location:
                        vtkSMTransferFunctionProxy.RescaleTransferFunction(lutProxy.SMProxy, array['range'][0], array['range'][1], False)
            
            view = simple.FindView(self.ds.views[viewIndex].name)
            simple.Render(view)
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.mslice.enable")
    def enableSlice(self, enable,viewIndex):
        if enable and self.ds.getInput(viewIndex):
            inpt = self.ds.getInput(viewIndex) if not self.useClippedInput[viewIndex] else self.clip.getOutput(viewIndex)
            if not self.slice[viewIndex]:
                self.slice[viewIndex] = simple.Slice(Input=inpt)
                normal = [0, 0, 0]
                normal[self.normal[viewIndex]] = 1
                self.slice[viewIndex].SliceType.Normal = normal
                self.slice[viewIndex].SliceOffsetValues = self.slicePositions[viewIndex]
                view = simple.FindView(self.ds.views[viewIndex].name)
                self.representation[viewIndex] = simple.Show(self.slice[viewIndex],view)
                self.representation[viewIndex].Representation = self.reprMode[viewIndex]
                self.representation[viewIndex].DiffuseColor = self.ds.foreground
                self.updateColorBy(self.colorBy[viewIndex][1], self.colorBy[viewIndex][0],viewIndex)
            else:
                self.slice[viewIndex].Input = inpt
            self.representation[viewIndex].Visibility = 1

        if not enable and self.representation[viewIndex]:
            self.representation[viewIndex].Visibility = 0

        view = simple.FindView(self.ds.views[viewIndex].name)
        simple.Render(view)
        self.getApplication().InvokeEvent('UpdateEvent')

# =============================================================================
#
# Streamline management
#
# =============================================================================

class LightVizStreamline(pv_protocols.ParaViewWebProtocol):

    def __init__(self, dataset_manager):
        super(LightVizStreamline, self).__init__()
        self.ds = dataset_manager
        self.streamline = [None,None]
        self.tube = [None,None]
        self.seed = [None,None]
        self.seedRep = [None,None]
        self.position = [[ 0.0, 0.0, 0.0 ],[ 0.0, 0.0, 0.0 ]]
        self.representation = [None,None]
        self.reprMode = ['Surface','Surface']
        self.colorBy = [('__SOLID__', '__SOLID__'),('__SOLID__', '__SOLID__')]
        self.vector = [None,None]
        self.numPoints = [50,50]
        self.radius = [1.0,1.0]
        dataset_manager.addListener(self)

    def dataChanged(self,viewIndex):
        print("LightVizStreamline:dataChanged 1 viewIndex" + str(viewIndex))
        bounds = self.ds.views[viewIndex].activeMeta['data']['bounds']
        length = [bounds[i*2+1] - bounds[i*2] for i in range(3)]
        print("LightVizStreamline:dataChanged 2 viewIndex" + str(viewIndex))
        self.updateRepresentation('Surface',viewIndex)
        self.updateColorBy('__SOLID__', '__SOLID__',viewIndex)
        self.vector[viewIndex] = None
        print("LightVizStreamline:dataChanged 3 viewIndex" + str(viewIndex))
        self.updateRadius(min(length) / 8.0, viewIndex)
        self.updatePosition(viewIndex,(bounds[1] + bounds[0])/2.0,
                            (bounds[3] + bounds[2])/2.0,
                            (bounds[5] + bounds[4])/2.0)
        print("LightVizStreamline:dataChanged 4 viewIndex" + str(viewIndex))
        for array in self.ds.views[viewIndex].activeMeta['data']['arrays']:
            if array['dimension'] == 3:
                self.vector[viewIndex] = array['name']
                break
        if self.streamline[viewIndex]:
            self.streamline[viewIndex].Input = self.ds.getInput(viewIndex)
        if self.representation[viewIndex]:
            self.representation[viewIndex].Visibility = 0
        print("LightVizStreamline:dataChanged 5 viewIndex" + str(viewIndex))

    def setForegroundColor(self, foreground,viewIndex):
        if self.representation[viewIndex]:
            self.representation[viewIndex].DiffuseColor = foreground

    @exportRpc("light.viz.streamline.getstate")
    def getState(self,viewIndex):
        print("streamline getState 1");
        try:
          ret = {
              "representation": self.reprMode[viewIndex],
              "color": self.colorBy[viewIndex],
              "enabled": False,
              "xPosition": self.position[viewIndex][0],
              "yPosition": self.position[viewIndex][1],
              "zPosition": self.position[viewIndex][2],
              "vector": self.vector[viewIndex],
              "numPoints": self.numPoints[viewIndex],
              "radius": self.radius[viewIndex],
          }
          if self.representation[viewIndex]:
              ret["enabled"] = self.representation[viewIndex].Visibility == 1,

          if not isinstance(ret["enabled"], bool):
              ret["enabled"] = ret["enabled"][0]
                 
          return ret
        except AttributeError:
           traceback.print_exc()
        except:
           print "Unexpected error light.viz.streamline.getstate:", sys.exc_info()[0]
       

    @exportRpc("light.viz.streamline.position")
    def updatePosition(self,viewIndex, x, y, z):
        print("streamline updatePosition 1")
        self.position[viewIndex] = [x, y, z]
        if self.streamline[viewIndex]:
            self.seed[viewIndex].Center = self.position[viewIndex]
            self.streamline[viewIndex].SeedType.Center = self.position[viewIndex]
            self.getApplication().InvokeEvent('UpdateEvent')
        print("streamline updatePosition 2")

    @exportRpc("light.viz.streamline.vector")
    def updateVector(self, vectorName,viewIndex):
        self.vector[viewIndex] = vectorName
        if self.streamline[viewIndex]:
            self.streamline[viewIndex].Vectors = ['POINTS', self.vector[viewIndex]]
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.streamline.numpoints")
    def updateNumPoints(self, num,viewIndex):
        self.numPoints[viewIndex] = int(num)
        if self.streamline[viewIndex]:
            self.streamline[viewIndex].SeedType.NumberOfPoints = self.numPoints[viewIndex]
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.streamline.radius")
    def updateRadius(self, rad, viewIndex):
        self.radius[viewIndex] = float(rad)
        if self.streamline[viewIndex]:
            self.seed[viewIndex].Radius = self.radius[viewIndex]
            self.streamline[viewIndex].SeedType.Radius = self.radius[viewIndex]
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.streamline.representation")
    def updateRepresentation(self, mode, viewIndex):
        self.reprMode[viewIndex] = mode
        if self.representation[viewIndex]:
            self.representation[viewIndex].Representation = mode
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.streamline.color")
    def updateColorBy(self, field, location,viewIndex):
        print("streamline updateColorBy 1")
        self.colorBy[viewIndex] = (location, field)
        if self.representation[viewIndex]:
            if field == '__SOLID__':
                print("streamline updateColorBy 2")
                self.representation[viewIndex].ColorArrayName = ''
            else:
                print("streamline updateColorBy 3")
                simpleColorBy(self.representation[viewIndex], self.colorBy[viewIndex])
                print("streamline updateColorBy 4")
                # Update data array range
                lutProxy = self.representation[viewIndex].LookupTable
                for array in self.ds.views[viewIndex].activeMeta['data']['arrays']:
                    if array['name'] == field and array['location'] == location:
                        vtkSMTransferFunctionProxy.RescaleTransferFunction(lutProxy.SMProxy, array['range'][0], array['range'][1], False)
            print("streamline updateColorBy 5")
            view = simple.FindView(self.ds.views[viewIndex].name)                                                                                                                          
            simple.Render(view)
            print("streamline updateColorBy 6")
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.streamline.enable")
    def enableStreamline(self, enable,viewIndex):
        print("enableStreamline 1 index: " + str(viewIndex))
        if enable and self.ds.getInput(viewIndex):
            print("enableStreamline 1.5")
            if not self.streamline[viewIndex]:
                print("enableStreamline 2")
                bounds = self.ds.views[viewIndex].activeMeta['data']['bounds']
                length = [bounds[i*2+1] - bounds[i*2] for i in range(3)]
                self.streamline[viewIndex] = simple.StreamTracer(Input=self.ds.getInput(viewIndex), SeedType='Point Source')
                self.streamline[viewIndex].Vectors = ['POINTS', self.vector[viewIndex]]
                self.streamline[viewIndex].MaximumStreamlineLength = 2 * max(length)
                print("enableStreamline 2.1")
                self.streamline[viewIndex].SeedType.Center = self.position[viewIndex]
                self.streamline[viewIndex].SeedType.Radius = self.radius[viewIndex]
                self.streamline[viewIndex].SeedType.NumberOfPoints = self.numPoints[viewIndex]

                if not self.seed[viewIndex]:
                  print("enableStreamline 2.2")
                  self.seed[viewIndex] = simple.Sphere()
                  self.seed[viewIndex].Center = self.position[viewIndex]
                  self.seed[viewIndex].Radius = self.radius[viewIndex]
                  print("enableStreamline 2.3")
                  view = simple.FindView(self.ds.views[viewIndex].name)
                  print("enableStreamline 2.4")
                  self.seedRep[viewIndex] = simple.Show(self.seed[viewIndex],view)
                  print("enableStreamline 2.4.1")
                  self.seedRep[viewIndex].Representation = 'Wireframe'
                  self.seedRep[viewIndex].DiffuseColor = [1, 1, 1]
                  self.seedRep[viewIndex].Visibility = 0
                  print("enableStreamline 2.5")
                
                print("enableStreamline 2.6")
                view = simple.FindView(self.ds.views[viewIndex].name)
                self.tube[viewIndex] = simple.Tube(view,Input=self.streamline[viewIndex])
                self.tube[viewIndex].Capping = 1
                self.tube[viewIndex].Radius = min(length) / 300.0
                print("enableStreamline 2.7")

            else:
                print("enableStreamline 1.8")
                self.streamline[viewIndex].Input = self.ds.getInput(viewIndex)

            if not self.representation[viewIndex]:
                print("enableStreamline 3")
                view = simple.FindView(self.ds.views[viewIndex].name)
                self.representation[viewIndex] = simple.Show(self.tube[viewIndex],view)
                print("enableStreamline 3.1")
                self.representation[viewIndex].Representation = self.reprMode[viewIndex]
                self.representation[viewIndex].DiffuseColor = self.ds.foreground
                self.updateColorBy(self.colorBy[viewIndex][1], self.colorBy[viewIndex][0],viewIndex)
                print("enableStreamline 3.2")

            self.representation[viewIndex].Visibility = 1

        if not enable and self.representation[viewIndex]:
            print("enableStreamline 4")
            self.representation[viewIndex].Visibility = 0

        print("enableStreamline 5")
        view = simple.FindView(self.ds.views[viewIndex].name)
        if view:
         print("enableStreamline 6")
         simple.Render(view)
         self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.streamline.seed.show")
    def showSeed(self, enable, viewIndex):
        try:
          print("streamline showSeed 1")
          if enable and self.ds.getInput(viewIndex):
              print("streamline showSeed 2")
              self.seedRep[viewIndex].Visibility = 1
          elif (not enable) and self.seedRep[viewIndex]:
              print("streamline showSeed 4")
              self.seedRep[viewIndex].Visibility = 0

          self.getApplication().InvokeEvent('UpdateEvent')
          print("streamline showSeed 5")
        except AttributeError:
          traceback.print_exc()
        except:
          print "Unexpected error light.viz.streamline.getstate:", sys.exc_info()[0]

    @exportRpc("light.viz.streamline.seed.update")
    def updateSeed(self, position, radius,viewIndex):
      print("streamline updateSeed 1 index: " + str(viewIndex))
      if self.seed[viewIndex]:
        print("streamline updateSeed 2")
        self.seed[viewIndex].Center = position
        self.seed[viewIndex].Radius = radius
        self.getApplication().InvokeEvent('UpdateEvent')
        print("streamline updateSeed 3")

# =============================================================================
#
# Volume management
#
# =============================================================================

class LightVizVolume(pv_protocols.ParaViewWebProtocol):

    def __init__(self, dataset_manager, clip):
        super(LightVizVolume, self).__init__()
        self.ds = dataset_manager
        self.clip = clip
        self.passThrough = None
        self.representation = None
        self.colorBy = ('__SOLID__', '__SOLID__')
        self.useClippedInput = False
        #dataset_manager.addListener(self)

    def dataChanged(self,viewIndex):
        self.updateColorBy(self.ds.activeMeta["data"]["arrays"][0]["name"], self.ds.activeMeta["data"]["arrays"][0]["location"])
        if self.passThrough:
            simple.Delete(self.passThrough)
            self.passThrough = None
            simple.Delete(self.representation)
            self.representation = None

    def setForegroundColor(self, foreground):
        pass

    @exportRpc("light.viz.volume.useclipped")
    def setUseClipped(self, useClipped):
        if self.useClippedInput != useClipped:
            self.useClippedInput = useClipped
            if self.passThrough:
                oldVisibility = self.representation.Visibility
                simple.Delete(self.representation);
                self.representation = None
                simple.Delete(self.passThrough);
                self.passThrough = None
                self.enableVolume(oldVisibility)


                self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.volume.getstate")
    def getState(self):
        ret = {
            'enabled': False,
            'color': self.colorBy,
            "use_clipped": self.useClippedInput,
        }
        if self.representation:
            ret["enabled"] = True if self.representation.Visibility else False
        return ret

    @exportRpc("light.viz.volume.representation")
    def updateRepresentation(self, mode):
        pass # It is a volume rendering, so that is the only valid representation

    @exportRpc("light.viz.volume.color")
    def updateColorBy(self, field, location):
        self.colorBy = (location, field)
        if self.representation:
            if field == '__SOLID__':
                self.representation.ColorArrayName = ''
            else:
                simpleColorBy(self.representation, self.colorBy)
                # Update data array range
                lutProxy = self.representation.LookupTable
                pwfProxy = self.representation.ScalarOpacityFunction
                for array in self.ds.activeMeta['data']['arrays']:
                    if array['name'] == field and array['location'] == location:
                        vtkSMTransferFunctionProxy.RescaleTransferFunction(lutProxy.SMProxy, array['range'][0], array['range'][1], False)
                        if pwfProxy:
                          vtkSMTransferFunctionProxy.RescaleTransferFunction(pwfProxy.SMProxy, array['range'][0], array['range'][1], False)

            simple.Render()
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.volume.enable")
    def enableVolume(self, enable,viewIndex):
        if enable and self.ds.getInput():
            inpt = self.ds.getInput() if not self.useClippedInput else self.clip.getOutput()
            if not self.passThrough:
                self.passThrough = simple.Calculator(Input=inpt)
            else:
                self.passThrough.Input = inpt
            if not self.representation:
                self.representation = simple.Show(self.passThrough)
                self.representation.Representation = 'Volume'
                self.updateColorBy(self.colorBy[1], self.colorBy[0])
            self.representation.Visibility = 1

        if not enable and self.representation:
            self.representation.Visibility = 0

        simple.Render()
        self.getApplication().InvokeEvent('UpdateEvent')

# =============================================================================
#
# Threshold management
#
# =============================================================================

class LightVizThreshold(pv_protocols.ParaViewWebProtocol):

    def __init__(self, dataset_manager, clip):
        super(LightVizThreshold, self).__init__()
        self.ds = dataset_manager
        self.clip = clip
        self.thresh = None
        self.representation = None
        self.colorBy = ('__SOLID__', '__SOLID__')
        self.useClippedInput = False
        self.rangeMin = 0
        self.rangeMax = 1
        self.thresholdBy = None
        #dataset_manager.addListener(self)

    def dataChanged(self,viewIndex):
        self.rangeMin = self.ds.activeMeta['data']['arrays'][0]['range'][0]
        self.rangeMax = self.ds.activeMeta['data']['arrays'][0]['range'][1]
        self.thresholdBy = self.ds.activeMeta['data']['arrays'][0]['name']
        if self.thresh:
            simple.Delete(self.thresh)
            self.thresh = None
        if self.representation:
            simple.Delete(self.representation)
            self.representation = None
        self.updateColorBy('__SOLID__', '__SOLID__')

    def setForegroundColor(self, foreground):
        if self.representation:
            self.representation.DiffuseColor = foreground

    @exportRpc("light.viz.threshold.useclipped")
    def setUseClipped(self, useClipped):
        if self.useClippedInput != useClipped:
            self.useClippedInput = useClipped
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.threshold.getstate")
    def getState(self):
        ret = {
            'enabled': False,
            'color': self.colorBy,
            'rangeMin': self.rangeMin,
            'rangeMax': self.rangeMax,
            'thresholdBy': self.thresholdBy,
            'use_clipped': self.useClippedInput,
        }
        if self.representation:
            ret["enabled"] = True if self.representation.Visibility else False
        return ret

    @exportRpc("light.viz.threshold.representation")
    def updateRepresentation(self, mode):
        self.reprMode = mode
        if self.representation:
            self.representation.Representation = mode
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.threshold.range")
    def updateRange(self, rangeMin, rangeMax):
        self.rangeMin = rangeMin
        self.rangeMax = rangeMax
        if self.thresh:
            self.thresh.ThresholdRange = [self.rangeMin, self.rangeMax]
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.threshold.color")
    def updateColorBy(self, field, location):
        self.colorBy = (location, field)
        if self.representation:
            if field == '__SOLID__':
                self.representation.ColorArrayName = ''
            else:
                simpleColorBy(self.representation, self.colorBy)
                # Update data array range
                lutProxy = self.representation.LookupTable
                pwfProxy = self.representation.ScalarOpacityFunction
                for array in self.ds.activeMeta['data']['arrays']:
                    if array['name'] == field and array['location'] == location:
                        vtkSMTransferFunctionProxy.RescaleTransferFunction(lutProxy.SMProxy, array['range'][0], array['range'][1], False)
                        if pwfProxy:
                          vtkSMTransferFunctionProxy.RescaleTransferFunction(pwfProxy.SMProxy, array['range'][0], array['range'][1], False)

            simple.Render()
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.threshold.by")
    def updateThresholdBy(self, field):
        self.thresholdBy = field
        if self.thresh:
            self.thresh.Scalars = ['POINTS', field]
            # simple.SaveState('/tmp/myteststate.pvsm')
            self.getApplication().InvokeEvent('UpdateEvent')

    @exportRpc("light.viz.threshold.enable")
    def enableThreshold(self, enable, viewIndex):
        if enable and self.ds.getInput():
            inpt = self.ds.getInput() if not self.useClippedInput else self.clip.getOutput()
            if not self.thresh:
                self.thresh = simple.Threshold(Input=inpt)
                self.thresh.ThresholdRange = [ self.rangeMin, self.rangeMax ]
                self.thresh.Scalars = ['POINTS', self.thresholdBy]
                self.representation = simple.Show(self.thresh)
            self.representation.Visibility = 1

        if not enable and self.representation:
            self.representation.Visibility = 0

        simple.Render()
        self.getApplication().InvokeEvent('UpdateEvent')


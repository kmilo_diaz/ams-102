import equals from 'mout/src/array/equals';

import { getOpacityMap, updateOpacityMap } from '../../client';

let arrayData = {};
const toNotify = {};
let counter =  0;

function updateOpacityPoints(name, points,index) {
  let indexName = name + index;
  if (!arrayData[indexName] || !equals(arrayData[indexName], points)) {
    arrayData[indexName] = points;
    if (toNotify[indexName] && toNotify[indexName].length > 0) {
      toNotify[indexName].forEach((obs) => obs.opacityPointsUpdated(name, points));
    }
  }
}

function convertAndUpdateOpacityPoints(name, points,index) {
  if (!points) {
    return;
  }
  const pts = [];
  for (let i = 0; i < points.length - 1; i += 2) {
    pts.push({ x: points[i], y: points[i + 1] });
  }
  updateOpacityPoints(name, pts,index);
}

export function setOpacityPoints(name, points, viewIndex) {
  updateOpacityPoints(name, points,viewIndex);
  updateOpacityMap(name, points, viewIndex);
}

export function updateArrayRange(name, range, index) {
  let indexName = name + index;
  console.log("updateArrayRange name:" + name + " index: "+ index+" indexName: "+indexName);
  if (toNotify[indexName] && toNotify[indexName].length > 0) {
    console.log("toNotify["+ indexName+ "].length " + toNotify[indexName].length);
    toNotify[indexName].forEach((obs) => {   
      obs.arrayRangeUpdated(name, range)}
    );
  }
}

export function initializeArrayOpacities(arrays,index) {
  let indexName = arrays.name + index;
   console.log("initializeArrayOpacities arrays.name: "+ arrays.name + " index: "+index + " indexName: "+indexName );
  arrayData = {};
  arrays.forEach((arr, idx) => {
    arrayData[indexName] = [];
    toNotify[indexName] = [];
    getOpacityMap(
      arr.name,
      convertAndUpdateOpacityPoints.bind(undefined, indexName,undefined,index),
      index
    );
  });
}

export function getOpacityPoints(name, index) {
  let indexName = name + index;
  return arrayData[indexName];
}

export function addOpacityMapObserver(name, obs, index) {
   
  
  let indexName = name + index;
   console.log("addOpacityMapObserver index: "+indexName );

  if (name) {
    if (!toNotify[indexName]) {
      console.log("toNotify["+ indexName + "] created: in view "+ index);
      toNotify[indexName] = [];
    }
    toNotify[indexName].push(obs);
    //console.log("toNotify: "+ toNotify);
  }
}

export function removeOpacityMapObserver(name, obs, index) {
   let indexName = name + index;
  if (indexName) {
    toNotify[indexName] = toNotify[indexName].filter((obj) => obj === obs);
  }
}

export default {
  setOpacityPoints,
  updateArrayRange,
  initializeArrayOpacities,
  getOpacityPoints,
  addOpacityMapObserver,
  removeOpacityMapObserver,
};

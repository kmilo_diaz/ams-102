import React from 'react';
import PropTypes from 'prop-types';
import equals from 'mout/src/array/equals';
import ColorMapEditorWidget from 'paraviewweb/src/React/Widgets/ColorMapEditorWidget';
import CollapsibleWidget from 'paraviewweb/src/React/Widgets/CollapsibleWidget';
import DropDownWidget from 'paraviewweb/src/React/Widgets/DropDownWidget';

import style from 'LightVizStyle/ColorMapWidget.mcss';

import {
  setColorMapPreset,
  getColorMap,
  listColorMapImages,
  setColorMapRange,
  setColorMapRangeToDataRange,
  getOpacityMap, 
  updateOpacityMap
} from '../../client';

/*import {
  setOpacityPoints,
  updateArrayRange,
  initializeArrayOpacities,
  getOpacityPoints,
  addOpacityMapObserver,
  removeOpacityMapObserver,
} from './OpacityMapSyncer';*/

export default class ColorMap extends React.Component {
  constructor(props) {
    super(props);

   //----
    
    this.arrayData = {};
    this.toNotify = {};
    this.counter =  0;

    this.updateOpacityPoints = this.updateOpacityPoints.bind(this);
    this.convertAndUpdateOpacityPoints = this.convertAndUpdateOpacityPoints.bind(this);
    this.setOpacityPoints = this.setOpacityPoints.bind(this);
    this.updateArrayRange = this.updateArrayRange.bind(this);
    this.initializeArrayOpacities = this.initializeArrayOpacities.bind(this);
    this.getOpacityPoints = this.getOpacityPoints.bind(this);
    this.addOpacityMapObserver = this.addOpacityMapObserver.bind(this);
    this.removeOpacityMapObserver = this.removeOpacityMapObserver.bind(this);

   //--

    console.log("ColorMap:constructor " + this.props.index);
    this.initializeArrayOpacities(this.props.dataset.data.arrays,this.props.index);
    this.addOpacityMapObserver(props.dataset.data.arrays[0].name, this,this.props.index );
    this.state = {
      currentColorMap: 'Cool to Warm',
      currentArray: 0,
      editColorMapMode: false,
      editOpacityMap: false,
      rangeMin: props.dataset.data.arrays[0].range[0],
      rangeMax: props.dataset.data.arrays[0].range[1],
      presets: {},
      points: [
        { x: props.dataset.data.arrays[0].range[0], y: 0.0 },
        { x: props.dataset.data.arrays[0].range[1], y: 1.0 },
      ],
    };

    this.onPieceWiseEditorChanged = this.onPieceWiseEditorChanged.bind(this);
    this.setRangeToDataRange = this.setRangeToDataRange.bind(this);
    this.setRangeToDataRangeOverTime = this.setRangeToDataRangeOverTime.bind(this);
    this.setPreset = this.setPreset.bind(this);
    this.setRange = this.setRange.bind(this);
    this.updateActiveArray = this.updateActiveArray.bind(this);
    this.opacityPointsUpdated = this.opacityPointsUpdated.bind(this);
    this.arrayRangeUpdated = this.arrayRangeUpdated.bind(this);
    this.loadColorMap = this.loadColorMap.bind(this);
    this.rangeUpdatedByServer = this.rangeUpdatedByServer.bind(this);

 
  }

  componentWillMount() {
    listColorMapImages((presets) => {
      this.setState({ presets });
    });
  }

  componentWillReceiveProps(newProps) {
    if (!equals(newProps.dataset.data.arrays, this.props.dataset.data.arrays)) {
      this.removeOpacityMapObserver(
        this.props.dataset.data.arrays[this.state.currentArray].name,
        this,this.props.index
      );
      this.initializeArrayOpacities(newProps.dataset.data.arrays,this.props.index);
      const newState = { currentArray: this.state.currentArray };
      if (this.state.currentArray >= newProps.dataset.data.arrays.length) {
        newState.currentArray = 0;
      }
      this.addOpacityMapObserver(
        newProps.dataset.data.arrays[newState.currentArray].name,
        this,this.props.index
      );
      const min = newProps.dataset.data.arrays[newState.currentArray].range[0];
      const max = newProps.dataset.data.arrays[newState.currentArray].range[1];
      if (this.state.rangeMin < min || this.state.rangeMin > max) {
        newState.rangeMin = min;
      }
      if (this.state.rangeMax < min || this.state.rangeMax > max) {
        newState.rangeMax = max;
      }
      this.setState(newState);
      getColorMap(
        newProps.dataset.data.arrays[newState.currentArray].name, this.props.index,
        this.loadColorMap
      );
    }
  }

  onPieceWiseEditorChanged(controlPoints) {
    if (!equals(controlPoints, this.state.points)) {
      this.setOpacityPoints(
        this.props.dataset.data.arrays[this.state.currentArray].name,
        controlPoints, this.props.index
      );
    }
  }

  setRangeToDataRange() {
    console.log("setRangeToDataRange: " + this.props.index);
    setColorMapRangeToDataRange(
      this.props.dataset.data.arrays[this.state.currentArray].name,
      this.rangeUpdatedByServer,
      this.props.index
    );
  }

  setRangeToDataRangeOverTime() {
    // The minimum and maximum recorded in the json file are the min/max over time
    const min = this.props.dataset.data.arrays[this.state.currentArray]
      .range[0];
    const max = this.props.dataset.data.arrays[this.state.currentArray]
      .range[1];
    setColorMapRange(
      this.props.dataset.data.arrays[this.state.currentArray].name,
      [min, max],this.props.index
    );
    this.updateArrayRange(
      this.props.dataset.data.arrays[this.state.currentArray].name,
      [min, max],this.props.index
    );
    this.setState({ rangeMin: min, rangeMax: max });
  }

  setPreset(name) {
    this.setState({ currentColorMap: name });
    setColorMapPreset(
      this.props.dataset.data.arrays[this.state.currentArray].name,
      name, this.props.index
    );
  }

  setRange(range) {
     console.log("setRange: " + range + " index: " +this.props.index);
    this.setState({ rangeMin: range[0], rangeMax: range[1] });
    this.updateArrayRange(
      this.props.dataset.data.arrays[this.state.currentArray].name,
      range,this.props.index
    );
    setColorMapRange(
      this.props.dataset.data.arrays[this.state.currentArray].name,
      range,this.props.index
    );
  }

  updateActiveArray(arrayLocLabel) {
    const [location, label] = arrayLocLabel.split(': ');
    let currentArray = -1;
    this.props.dataset.data.arrays.forEach((item, index) => {
      if (currentArray !== -1) {
        return;
      }
      if (item.label === label && item.location === location) {
        currentArray = index;
      }
    });

    const newState = { currentArray };
    const min = this.props.dataset.data.arrays[newState.currentArray].range[0];
    const max = this.props.dataset.data.arrays[newState.currentArray].range[1];
    this.removeOpacityMapObserver(
      this.props.dataset.data.arrays[this.state.currentArray].name,
      this, this.props.index
    );
    if (this.state.rangeMin < min || this.state.rangeMin > max) {
      newState.rangeMin = min;
    }
    if (this.state.rangeMax < min || this.state.rangeMax > max) {
      newState.rangeMax = max;
    }
    this.addOpacityMapObserver(
      this.props.dataset.data.arrays[newState.currentArray].name,
      this,this.props.index
    );
    newState.points = this.getOpacityPoints(
      this.props.dataset.data.arrays[newState.currentArray].name,this.props.index
    );
    this.setState(newState);
    getColorMap(
      this.props.dataset.data.arrays[newState.currentArray].name,this.props.index,
      this.loadColorMap
    );
  }

  opacityPointsUpdated(name, points) {
    this.setState({ points });
  }


  arrayRangeUpdated(name, range) {
     console.log("arrayRangeUpdated this.props.index: "+ this.props.index);
    this.setState({ rangeMin: range[0], rangeMax: range[1] });
  }

  loadColorMap(preset, range) {
    const newState = {
      currentColorMap: preset,
      rangeMin: range[0],
      rangeMax: range[1],
    };
    newState.points = this.getOpacityPoints(
      this.props.dataset.data.arrays[this.state.currentArray].name,this.props.index
    );
    this.setState(newState);
  }

  rangeUpdatedByServer(newRange) {
    this.setState({ rangeMin: newRange[0], rangeMax: newRange[1] });
    this.updateArrayRange(
      this.props.dataset.data.arrays[this.state.currentArray].name,
      newRange
    );
  }

//--------------------------------

 updateOpacityPoints(name, points,index) {
  let indexName = name + index;
  if (!this.arrayData[indexName] || !equals(this.arrayData[indexName], points)) {
    this.arrayData[indexName] = points;
    if (this.toNotify[indexName] && this.toNotify[indexName].length > 0) {
      this.toNotify[indexName].forEach((obs) => obs.opacityPointsUpdated(name, points));
    }
  }
}

 convertAndUpdateOpacityPoints(name, points,index) {
  if (!points) {
    return;
  }
  const pts = [];
  for (let i = 0; i < points.length - 1; i += 2) {
    pts.push({ x: points[i], y: points[i + 1] });
  }
  this.updateOpacityPoints(name, pts,index);
}

  setOpacityPoints(name, points, viewIndex) {
  this.updateOpacityPoints(name, points,viewIndex);
  updateOpacityMap(name, points, viewIndex);
}

  updateArrayRange(name, range, index) {
  let indexName = name + index;
  console.log("updateArrayRange name:" + name + " index: "+ index+" indexName: "+indexName);
  if (this.toNotify[indexName] && this.toNotify[indexName].length > 0) {
    console.log("toNotify["+ indexName+ "].length " + this.toNotify[indexName].length);
    this.toNotify[indexName].forEach((obs) => {   
      obs.arrayRangeUpdated(name, range)}
    );
  }
}

  initializeArrayOpacities(arrays,index) {
  let indexName = arrays.name + index;
   console.log("initializeArrayOpacities arrays.name: "+ arrays.name + " index: "+index + " indexName: "+indexName );
  this.arrayData = {};
  arrays.forEach((arr, idx) => {
    this.arrayData[indexName] = [];
    this.toNotify[indexName] = [];
    getOpacityMap(
      arr.name,
      this.convertAndUpdateOpacityPoints.bind(undefined, indexName,undefined,index),
      index
    );
  });
}

  getOpacityPoints(name, index) {
  let indexName = name + index;
  return this.arrayData[indexName];
}

  addOpacityMapObserver(name, obs, index) {
   
  
  let indexName = name + index;
   console.log("addOpacityMapObserver index: "+indexName );

  if (name) {
    if (!this.toNotify[indexName]) {
      console.log("toNotify["+ indexName + "] created: in view "+ index);
      this.toNotify[indexName] = [];
    }
    this.toNotify[indexName].push(obs);
    //console.log("toNotify: "+ toNotify);
  }
}

  removeOpacityMapObserver(name, obs, index) {
   let indexName = name + index;
  if (indexName) {
     this.toNotify[indexName] = this.toNotify[indexName].filter((obj) => obj === obs);
  }
}


  render() {
    // Stupid guard as DropDownWidget is not aware when its prop change
    if (
      !this.props.dataset ||
      !this.props.dataset.data ||
      !this.props.dataset.data.arrays ||
      !this.props.dataset.data.arrays.length ||
      !this.props.dataset.data.arrays[0].label
    ) {
      return null;
    }

    const min = this.props.dataset.data.arrays[this.state.currentArray]
      .range[0];
    const max = this.props.dataset.data.arrays[this.state.currentArray]
      .range[1];
    return (
      <CollapsibleWidget
        className={style.container}
        title="ColorMap"
        activeSubTitle
        disableCollapse={this.props.disableCollapse}
        subtitle={
          <DropDownWidget
            field={`${
              this.props.dataset.data.arrays[this.state.currentArray].location
            }: ${
              this.props.dataset.data.arrays[this.state.currentArray].label
            }`}
            fields={this.props.dataset.data.arrays.map(
              (item) => `${item.location}: ${item.label}`
            )}
            onChange={this.updateActiveArray}
          />
        }
      >
        <ColorMapEditorWidget
          currentPreset={this.state.currentColorMap}
          rangeMin={this.state.rangeMin}
          rangeMax={this.state.rangeMax}
          currentOpacityPoints={this.state.points}
          dataRangeMin={min}
          dataRangeMax={max}
          presets={this.state.presets}
          onOpacityTransferFunctionChanged={this.onPieceWiseEditorChanged}
          onPresetChanged={this.setPreset}
          onRangeEdited={this.setRange}
          onScaleRangeToCurrent={this.setRangeToDataRange}
          onScaleRangeOverTime={this.setRangeToDataRangeOverTime}
          pieceWiseHeight={150}
        />
      </CollapsibleWidget>
    );
  }
}

ColorMap.propTypes = {
  dataset: PropTypes.object,
  disableCollapse: PropTypes.bool,
};

ColorMap.defaultProps = {
  dataset: undefined,
  disableCollapse: false,
};

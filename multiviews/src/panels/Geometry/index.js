import DoubleSliderElement from 'paraviewweb/src/React/Widgets/DoubleSliderWidget';
import React from 'react';
import PropTypes from 'prop-types';

import style from 'LightVizStyle/DatasetPanel.mcss';

import AbstractPanel from '../AbstractPanel';

import {
  updateGeometryOpacity,
  getState,
} from '../../client';

export default class GeometryPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      oldOpacity: 1,
      opacity: 1,
    };
    this.onApply = this.onApply.bind(this);
    this.onReset = this.onReset.bind(this);
    this.updateState = this.updateState.bind(this);
    this.updateOpacity = this.updateOpacity.bind(this);

  }

  componentDidMount() {
    getState('geometry', this.props.index, this, this.modulePanel);
  }

  onApply() {
    updateGeometryOpacity(this.state.opacity, this.props.index);
    this.setState({ oldOpacity: this.state.opacity });
  }

  onReset() {
    this.setState({
      opacity: this.state.oldOpacity,
    });
  }

  updateState(newState) {
    this.setState({
      opacity: newState.opacity,
    });
  }

  updateOpacity(name, value) {
    this.setState({
      opacity: value,
    });
    if (this.props.dataset.autoApply) {
      updateGeometryOpacity(value, this.props.index);
    }
  }

  render() {
    return (
      <AbstractPanel
        ref={(c) => {
          this.modulePanel = c;
        }}
        name="geometry"
        dataset={this.props.dataset}
        hideAllButVisibility={this.props.hideAdditionalControls}
        hideInputSelection
        moduleName="Geometry"
        onApply={this.onApply}
        onReset={this.onReset}
        index={this.props.index}
        needsApply={this.state.opacity !== this.state.oldOpacity}
        enabledDefault={false}
      >
        <div className={style.contents}>
          <i className={style.opacityIcon} />
          <DoubleSliderElement
            min="0"
            max="1"
            value={this.state.opacity}
            onChange={this.updateOpacity}
          />
        </div>
      </AbstractPanel>
    );
  }
}

GeometryPanel.propTypes = {
  dataset: PropTypes.object,
  hideAdditionalControls: PropTypes.bool,
};

GeometryPanel.defaultProps = {
  dataset: undefined,
  hideAdditionalControls: false,
};

// ----------------------------------------------------------------------------
// This is the entry file of the front end application. When you invoke the path
// in the web browser, this is the first file it reads.
//
// ----------------------------------------------------------------------------
import React from 'react';
import ReactDOM from 'react-dom';

import ProgressLoaderWidget from 'paraviewweb/src/React/Widgets/ProgressLoaderWidget';
import { createClient } from 'paraviewweb/src/IO/WebSocket/ParaViewWebClient';
import { HashRouter as Router, Route, withRouter } from 'react-router-dom';

import SizeHelper from 'paraviewweb/src/Common/Misc/SizeHelper';

// import network from './network';
import MainView from './MainView';

import ListDatasets from "./pages/ListDatasets"
import ViewDataset from "./pages/ViewDataset"

import SmartConnect from 'wslink/src/SmartConnect';

// ----------------------------------------------------------------------------
// this is the pointer to the client.js file
// from this file you can invoke the imported methods
// ----------------------------------------------------------------------------
import { setup,getViews } from './client';

import {
  loadConfiguration,
  addConfigObserver,
  getActiveProfile,
} from './config';




// ----------------------------------------------------------------------------
// This determines the possble paths in the application. The main page is "/" and 
// "/list". If the browser request any of these paths, the application goes to ListDatasets/index.js 
// The dual vew web page is "/view/dual" goes to ViewDataset/index.js.
// ----------------------------------------------------------------------------
const routes = (
  <Router>
    <div>
      <Route exact path="/" component={withRouter(ListDatasets)} />
      <Route exact path="/list" component={withRouter(ListDatasets)} />
       <Route
        exact path="/view/dual"
        component={withRouter(ViewDataset)}
      />
    </div>
  </Router>
);

// ----------------------------------------------------------------------------
// This method starts the app. Connects to server, get the id views and render the routes
// component according to what is requested by the browser
// ----------------------------------------------------------------------------

function start(conn) {

   const client = createClient(conn, [
      'ColorManager',
      'FileListing',
      'MouseHandler',
      'ProxyManager',
      'TimeHandler',
      'ViewPort',
      'VtkImageDelivery',
    ]);
    setup(conn, client);
  


  loadConfiguration();

  SizeHelper.startListening(); 

  // Mount UI
  const container = document.querySelector('.content');

  ReactDOM.unmountComponentAtNode(container);    
  ReactDOM.render(routes, container);

}


function triggerError(sConnect, message = 'Server disconnected') {
  loading(message);
}

function loading(message = 'Loading ParaView...') {
  // Mount UI
  const container = document.querySelector('.content');
  ReactDOM.unmountComponentAtNode(container);
  ReactDOM.render(<ProgressLoaderWidget message={message} />, container);
}


// ----------------------------------------------------------------------------
// As soon as it loads the main page, it connects to the server in order to
// get the ids of the views created internally by paraview
// ----------------------------------------------------------------------------
export function connect(config = {}) {
  loading();
  const smartConnect = SmartConnect.newInstance({ config });
  smartConnect.onConnectionReady(start);
  smartConnect.onConnectionError(triggerError);
  smartConnect.onConnectionClose(triggerError);
  smartConnect.connect();
}

// ----------------------------------------------------------------------------
// method not used in the current project
// ----------------------------------------------------------------------------

function renderViews(id)
{
   
  const container = document.querySelector('.content');

    getViews()
    .then((viewIds) => {
      ReactDOM.unmountComponentAtNode(container);
       
      ReactDOM.render(
      
        <MainView viewIds={viewIds} viewToSynch={id} renderCallBack={renderViews}/>,
      
       container);  
     
    });



}


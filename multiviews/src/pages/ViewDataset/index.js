// ----------------------------------------------------------------------------
// This file renders the dual view of the datasets
// ----------------------------------------------------------------------------

import React from 'react';
import PropTypes from 'prop-types';
import clone from 'mout/src/lang/clone';
import VtkRenderer from 'paraviewweb/src/React/Renderers/VtkRenderer';
import SvgIcon from 'paraviewweb/src/React/Widgets/SvgIconWidget';
import ProgressLoaderWidget from 'paraviewweb/src/React/Widgets/ProgressLoaderWidget';

import style from 'LightVizStyle/ViewDataset2.mcss';

import SizeHelper from 'paraviewweb/src/Common/Misc/SizeHelper';

import modules from '../../modules';
import TimePanel from '../../panels/Time';
import ColorMap from '../../widgets/ColorMap';
import lightVizIcon from '../../../svg/LightViz.svg';

import TogglePanel from './TogglePanel'
import ParaViewRender from './ParaViewRender'
import { Link } from 'react-router-dom';

import {
  resetCamera,
  loadDataset,
  getConnection,
  getClient,
  saveThumbnail,
  setForegroundColor,
  setBackgroundColor,
  onBusyChange,
  unsubscribeBusy,
  setActiveView,
  linkCameras,
  unLinkCameras
} from '../../client';


function updateColors(e) {
  const idx = e.target.dataset.index;
  const scheme = colors[idx];
  setForegroundColor(scheme.pvcolor);
  setBackgroundColor(scheme.pvbgcolor);
}



export default class ViewDataset2 extends React.Component {

// ----------------------------------------------------------------------------
// This method is call the first time the web page is rendered. It has two dataset attributes dataset(1|2)
// each of these, have the data extracted from the dataset by the paraview application running on the server side.
// ----------------------------------------------------------------------------

  constructor(props) {
    super(props);

  
    // global state of the object
    this.state = {
      time: 0,
      
      
      // left dataset
      dataset1: {
        empty: true,
        data: {
          arrays: [
            { range: [0, 100], name: 'unkown', location: 'unkown', label: '' },
          ],
          time: [],
          bounds: [0, 1, 0, 1, 0, 1],
        },
      },
      datasetId: null,

      // right dataset
     dataset2: {
        empty: true,
        data: {
          arrays: [
            { range: [0, 100], name: 'unkown', location: 'unkown', label: '' },
          ],
          time: [],
          bounds: [0, 1, 0, 1, 0, 1],
        },
        autoApply: false,
      },
      dataset2Id: null,

      busyView1: false,
      busyView2: false,

      geometryView1Loaded : false,
      geometryView2Loaded : false,

      showControls: false,
      busy: false,

      fullView1: false,
      fullView2: false,

      view1Width: '50%',
      view2Width: '50%',

      rendererTwoVisible: true,

      dualView :true,
    };


    this.fullViewSize = '80%';
    this.normalViewSize = '50%';
    this.zeroViewSize = '80%';

    this.listDataSets = this.listDataSets.bind(this);
    this.toggleShowControls = this.toggleShowControls.bind(this);
    this.toggleAutoApply = this.toggleAutoApply.bind(this);

    this.subscriptionId = onBusyChange((busy) => this.setState({ busy }));

    this.renderViews = this.renderViews.bind(this);
    this.loadGeometry = this.loadGeometry.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.getViewById = this.getViewById.bind(this);


     this.renderCallBack = props.renderCallBack;
     this.areLinked = false;
     this.currentView = -1;
     this.viewsIds =  props.location.views;
    
     this.datasetnames = props.location.viewNames;
    
     this.profile = props.location.profile;

     this.linkCameras = this.linkCameras.bind(this);
     this.unLinkCameras = this.unLinkCameras.bind(this);
     this.setFullView = this.setFullView.bind(this);
     
     this.containers = ['renderContainerOne','renderContainerTwo'];

    
     this.resized = false;

     this.resize = this.resize.bind(this);
     this.resizeTimed = this.resizeTimed.bind(this);


    
  }
 

// ----------------------------------------------------------------------------
// At the moment of displaying this view (bedore rendering, after construction)
// It calls the render side to retrieve the data sets using the corresponding ids 
// from the server .
// ----------------------------------------------------------------------------
  componentWillMount() {


    if((this.viewsIds && this.viewsIds.length > 0) 
       &&  this.datasetnames.length > 0)
    {

      // here it fills the attributes dataset1 and dataset2
      this.viewsIds.map((id,index) => {
         loadDataset(index,this.datasetnames[index], (dataset) =>
                    {
                          if(index==0){
                             this.setState({
                                dataset1 : dataset,
                                geometryView1Loaded : true
                              });
                          }
                          else{
                              this.setState({
                                dataset2 : dataset,
                                geometryView2Loaded : true
                              });
                          }
                          
                    }
         );
      });
 
    }

  }

  componentWillUnmount() {
    unsubscribeBusy(this.subscriptionId);
  }



// ----------------------------------------------------------------------------
// go back to the listdatset web page
// ----------------------------------------------------------------------------
  listDataSets() {
    this.props.history.push('/list');
  }

// ----------------------------------------------------------------------------
// show / hide UI tabs 
// ----------------------------------------------------------------------------
  toggleShowControls(e) {

    const show = !this.state.showControls;
    this.setState({ showControls: show });

  }

// ----------------------------------------------------------------------------
// We dont use this method in this project
// ----------------------------------------------------------------------------
  toggleAutoApply() {
    const newAA = !this.state.dataset.autoApply;
    const newDataset = clone(this.state.dataset);
    newDataset.autoApply = newAA;
    this.setState({ dataset: newDataset });
  }


// ------------------------------------------------------------------------------------
// Handles the click on the UI. It makes the render view active in the paraview server
// -----------------------------------------------------------------------------------

 handleClick(event) {

    if (event.type === 'click') {
       //if(this.currentView != event.currentTarget.id)
       //{
         //this.currentView = event.currentTarget.id.replace("renderContainer",""));
         setActiveView(event.currentTarget.id.replace("renderContainer","")); 

       //}
     } else if (event.type === 'contextmenu') {
     
    }

  }

// ------------------------------------------------------------------------------------
// Deprecated method. It used to load the geometry the first time the web page is rendered
// -----------------------------------------------------------------------------------

  loadGeometry(viewIndex)
  {
    
    if(viewIndex == 0 && !this.state.geometryView1Loaded)
    {
        const dataId = 'mat-viz-mofTFF-90L-9.1lpm-100rpm.case';
        /* this.setState({
                        geometryView1Loaded : true,
                        dataset1Id: dataId,
                        busyView1: true
                       }); */
 

        loadDataset(viewIndex,dataId,(dataset) =>
                                                        this.setState({
                                                          dataset1 : dataset,
                                                          geometryView1Loaded : true
                                                        }));

        //this.renderCallBack(0); // re render but DO NOT link Cameras
    }
    else if(viewIndex == 1 && !this.state.geometryView2Loaded)
    {
        const dataId = 'mat-viz-mofTFF-90L-9.1lpm-100rpm.case';
        this.setState({
                        geometryView2Loaded : true,
                        dataset2Id: dataId,
                        busyView2: true
                       });

        loadDataset(viewIndex,dataId,(dataset) =>
                                                        this.setState({
                                                          dataset2 : dataset,
                                                          geometryView2Loaded: true
                                                        }));
       // this.renderCallBack(0); // re render but DO NOT link Cameras
    }
    
  }
  
// ------------------------------------------------------------------------------------
// Returns the render view id linked to the dataset
// -----------------------------------------------------------------------------------

  getViewById(index){
      if(index == 0)
      {  
          return this.state.dataset1;
      }
      return this.state.dataset2;
  }

// ------------------------------------------------------------------------------------
// Links the cameras. It sends a message to the paraview server to link cameras
// -----------------------------------------------------------------------------------

  linkCameras()
  {
    linkCameras(); 
    this.areLinked = true;
  }

// ------------------------------------------------------------------------------------
// unLinks the cameras. It sends a message to the paraview server to unlink cameras
// -----------------------------------------------------------------------------------
  unLinkCameras()
  {
    unLinkCameras();
    this.areLinked = false; 
  }

// ------------------------------------------------------------------------------------------------------------------------------
// sets the first view in full mode. 100 ms After clicking the "show/hide" button, method resizeTimed is called and do the logic
// ------------------------------------------------------------------------------------------------------------------------------
 setFullView()
 {
    
    setTimeout(this.resizeTimed, 100);
    let dView = this.state.dualView;
    this.setState ({
       dualView : !dView,
    });
 
 }

// ------------------------------------------------------------------------------------
// Deprecated method. It uses to handle the asynchronous call to resizeTimed
// -----------------------------------------------------------------------------------
 resize()
 {
    
   if(!this.resized)
   {
     setTimeout(this.resizeTimed, 100);
   }

 }

// ------------------------------------------------------------------------------------
// It calls the Paraview client API to resize the first view
// -----------------------------------------------------------------------------------
 resizeTimed()
 {

   SizeHelper.triggerChange();


  }

// ----------------------------------------------------------------------------------------------------
// This method checks when the daasets have been received from the sever, and it displays both views when the process is completed
// Once the datasets are loaded it renderes the views, panel options and buttons.
// -----------------------------------------------------------------------------------------------------
  renderViews(id,index)
  {


      if(this.state.dataset1.empty || this.state.dataset2.empty) 
      { 

          return ( <ProgressLoaderWidget key={'progressBar'+index} message="Server busy" /> );  
      }
      
      else if (!this.state.dataset1.empty && !this.state.dataset2.empty)
      {



          const divWidth1 = this.state.dualView ? '50%' : '100%';  
          const divWidth2 = '50%';  

          const divDisplay1 = 'table-cell';
          const divDisplay2 = this.state.dualView? 'table-cell' : 'none';

          return (  
                                         
                    <div key={'view'+id} style ={{ position: 'relative', display: index == 0? divDisplay1:divDisplay2,  padding: index==0?'10px':'0px', width: index==0? divWidth1:divWidth2,}} 
                         id={'renderContainer'+id} 
                         onClick={this.handleClick} >
                      <TogglePanel key={'togglePanel'+index} {...this.props} dataset={ index == 0 ? this.state.dataset1: this.state.dataset2} index={index} profile={this.profile} viewId={id}/> 
                      <ParaViewRender key={'renderer'+index} index={index}  id={id}  container={this.containers[index]}/>
                      <center><h1>{this.datasetnames[index]}</h1></center>
                   </div>
                 );  
      }

      return null;      

  }


   render() {

       return(
       
         <div>
             <div key={'buttonView'} >  <center> <button onClick={(e)=>this.setFullView() }>  {this.state.dualView? "hide" : "show"} second </button>  </center> </div>
             <div style={{ display: 'table',  width: '100%',  tableLayout: 'fixed' }}>
                <div style={{ display: 'table-row', }}>
                    {this.viewsIds.map((id,index) => { 
                          return ( this.renderViews(id,index));
                      }
                   )}
               </div>
             </div>
             <center>
               <div style={{display: 'inline-block'}}>
                 <button onClick={(e)=>this.linkCameras() }>Link Cameras</button> &nbsp;
                 <button onClick={(e)=>this.unLinkCameras() }>UnLink Cameras</button> 
               </div>
             </center>         
         </div>
       );

    }

}



ViewDataset2.propTypes = {
  profile: PropTypes.object,

};

ViewDataset2.defaultProps = {
  profile: undefined,
};

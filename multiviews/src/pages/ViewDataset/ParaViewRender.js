import React from 'react';
import PropTypes from 'prop-types';


import VtkRenderer from 'paraviewweb/src/NativeUI/Renderers/VtkRenderer';

import style from 'LightVizStyle/ViewDataset2.mcss';

import SizeHelper from 'paraviewweb/src/Common/Misc/SizeHelper';

import { getClient,
         getConnection 
       }from '../../client';

// ----------------------------------------------------------------------------
//  This component is a container for the render views
// ----------------------------------------------------------------------------

export default class VtkViewRenderer extends React.Component {

     constructor(props){
          super(props);
          this.state = {
               fullView : this.props.fullView,
          };

       this.setRenderers = this.setRenderers.bind(this);
       

       this.divRenderer =  <div id={this.props.container} style={{ height: '80vh', resize: 'both',overflow: 'hidden', zIndex: '10', }} />;
       this.resized = false;
       this.renderer = null;
       setTimeout(
               this.setRenderers, 500);
     }


   componentWillReceiveProps(nextProps) {

     this.setState({
             fullView: this.props.fullView,
      });
        
   }
  
   // ----------------------------------------------------------------------------
   //  This method is called 500 ms after the component is created. Because the render views are created first in the
   //  paraview server side, and the UI needs to wait until the server reports the views have been created.
   // ----------------------------------------------------------------------------
   setRenderers()
   {

          this.renderer = VtkRenderer.newInstance({
                  client: getClient(),
                  viewId: this.props.id,
                });
     
          this.renderer.setContainer(
                 document.getElementById(this.props.container)  );

          // This is invoked here in order to attach the mouse handler.
          this.renderer.setViewId(this.props.id);

        SizeHelper.onSizeChange(() => {
           this.renderer.resize();
    
        });


   }


   render(){
         return (this.divRenderer );
   }
}

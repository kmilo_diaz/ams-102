import React from 'react';
import PropTypes from 'prop-types';


import style from 'LightVizStyle/ViewDataset2.mcss';


import TogglePanelWidget from 'paraviewweb/src/React/Widgets/TogglePanelWidget';
import CollapsibleWidget from 'paraviewweb/src/React/Widgets/CollapsibleWidget';
import SvgIcon from 'paraviewweb/src/React/Widgets/SvgIconWidget';
import lightVizIcon from '../../../svg/LightViz.svg';

import ColorMap from '../../widgets/ColorMap';

/* List of UI panels  added to the UI*/
import modules from '../../modules';


import Section from '../../panels/Section';


import {
  resetCamera,
  setForegroundColor,
  setBackgroundColor,
  saveThumbnail,
  queryScreenShot,
  query3DModel,
} from '../../client';

const colors = [
  {
    label: 'White',
    pvcolor: '1 1 1',
    pvbgcolor: '0 0 0',
    cssColor: [255, 255, 255],
    cssBGColor: [0, 0, 0],
  },
  {
    label: 'Red',
    pvcolor: '.8 .2 .2',
    pvbgcolor: '.3 .3 .3',
    cssColor: [204, 51, 51],
    cssBGColor: [77, 77, 77],
  },
  {
    label: 'Green',
    pvcolor: '.2 .8 .2',
    pvbgcolor: '.4 .2 .2',
    cssColor: [51, 204, 51],
    cssBGColor: [102, 51, 51],
  },
];
 

// ----------------------------------------------------------------------------
//  This component represents the UI Panel on top of the render view.  
//
// ----------------------------------------------------------------------------

export default class TogglePanel extends React.Component {
   

    constructor(props)
    { 
       super(props);
       this.state = {
           showControls: false,
       };
       this.toggleShowControls = this.toggleShowControls.bind(this);
       this.listDataSets = this.listDataSets.bind(this);
       this.handleResetCameraClick = this.handleResetCameraClick.bind(this);
       this.updateColors = this.updateColors.bind(this);
       this.screenShot = this.screenShot.bind(this);
       this.downloadScreenShot = this.downloadScreenShot.bind(this);
       this.model3DExport = this.model3DExport.bind(this);
       const scheme = colors[0];
       setBackgroundColor(scheme.pvbgcolor,  this.props.index,0);

    }

// ----------------------------------------------------------------------------
//  Handles the event of hide/unhide the UI Panel
// ----------------------------------------------------------------------------
    toggleShowControls(e) {
      const show = !this.state.showControls;
      this.setState({ showControls: show });
    }

// ----------------------------------------------------------------------------
//  Handles the event of returning to the dataset selection webpage
// ----------------------------------------------------------------------------
    listDataSets() {
      this.props.history.push('/list');
    }

// ----------------------------------------------------------------------------
//  Handles the event of reseting the camera (It calls paraview server API)
// ----------------------------------------------------------------------------
    handleResetCameraClick(event) {
       resetCamera(event.currentTarget.id)
    }

// ----------------------------------------------------------------------------
// Method to take the screenshot
// ----------------------------------------------------------------------------
  screenShot() {
    /*window.confirm(
      "Add a snapshot of the current view ?\n" +
        'The view will be resized and the camera reset before taking the screenshot.'
    );*/

    // I'd like to ask the user if they are sure here...
    var fileName = prompt("Please enter a name for the image", "ScreenShot"+this.props.index);
    queryScreenShot(this.props.index,fileName, this.downloadScreenShot);
  }

// ----------------------------------------------------------------------------
// Download automatically the screenshot
// ----------------------------------------------------------------------------
  downloadScreenShot(filePath)
  {
    //window.location = 'my download url';
    console.log(filePath);
    window.confirm('Your file was saved at ' +filePath)
  }

// ----------------------------------------------------------------------------
// Export the model for VR
// ----------------------------------------------------------------------------

   model3DExport()
   {
     var fileName = prompt("Please enter a name for the model", "Model"+this.props.index);
    query3DModel(this.props.index,fileName, this.downloadScreenShot);
   }

// ----------------------------------------------------------------------------
//  Controls the background colors
// ----------------------------------------------------------------------------
    updateColors(e) {
      const idx = e.target.dataset.index;
      
      const scheme = colors[idx];

      setBackgroundColor(scheme.pvbgcolor,  this.props.index,idx);
    }

    render(){
        

            const controlPanelClass = this.state.showControls
                 ? style.controlPanel
                 : style.controlPanelHidden;
            
            const controlsClass = this.state.showControls
                 ? style.controls
                 : style.controlsHidden;


           // Do no remove the commented code below. It's important for future debug events or adding new tabs.
           const newModules = modules;/*.filter((mdl) => 
               {
                 if (this.props.profile.modules_included.includes(mdl.name)) 
                 {
                   return true;
                 }
                 if ( this.props.profile.modules_included.length === 0 && !this.props.profile.modules_excluded.includes(mdl.name)) 
                 {
                   return true;
                 }
                return false;
               });*/


         return(
                     
                     
                       <div key={'toggleInPanel'+this.props.viewId} className={controlPanelClass}>
                         <div className={style.showControlsIconDiv}>
                            <div className={style.lineItem}>
                               <i
                                 className={style.listDataSetsButton}
                                 onClick={this.listDataSets}
                               />
                               <i id={this.props.viewId} className={style.resetViewButton} onClick={this.handleResetCameraClick} />
                               <i className={ this.state.showControls ? style.addThumbnailButton : style.hidden } onClick={this.screenShot}/>
                               <i className={ this.state.showControls ? style.addThumbnailButton : style.hidden } onClick={this.model3DExport}/>
                               <div className={style.logo} onClick={this.toggleShowControls}>
                                  <SvgIcon
                                    icon={lightVizIcon}
                                    height="30px"
                                    width="30px"
                                    style={{ position: 'absolute', top: '-6px', left: 0 }}
                                  />
                               </div>
                            </div>
                            {colors.map((c, idx) => {
                                         const myfg = c.cssColor.join(',');
                                         const mybg = c.cssBGColor.join(',');
                                         const mygradient = `radial-gradient(rgb(${myfg}) 30%, rgb(${mybg}) 45%)`;
                                         return (
                                           <div
                                             className={
                                                   this.state.showControls ? style.colorItem : style.hidden
                                             }
                                             key={idx}
                                             style={{ background: mygradient }}
                                             data-index={idx}
                                             onClick={this.updateColors}
                                           />
                                         );
                            })}
                         </div>
                         <div className={controlsClass}>
                                <ColorMap dataset={this.props.dataset} index={this.props.index} />  
                                {newModules.map((panel) => (
                                 <Section
                                   className={style.section}
                                   key={panel.name}
                                   panel={panel}
                                   {...this.props}
                                   dataset={this.props.dataset}
                                   index={this.props.index} 
                                 />
                                ))}
                         </div>
                       </div>

           );

    }
    
}

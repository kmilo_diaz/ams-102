
// ----------------------------------------------------------------------------
// It represents the list view where the user gets to select the datasets to be 
// displayed in dual view
// ----------------------------------------------------------------------------


import React from 'react';
import SvgIcon from 'paraviewweb/src/React/Widgets/SvgIconWidget';

import style from 'LightVizStyle/ListDatasets.mcss';

import { listDatasets , getViews} from '../../client';
import ThumbnailList from './ThumbnailList';
import lightVizIcon from '../../../svg/LightViz.svg';

import { loadConfiguration, getActiveProfile,addConfigObserver } from '../../config';

import { Link } from 'react-router-dom';

export default class ListDatasets extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      datasetName1: '',
      datasetName2: '',
      datasets: [],
      views: [],
      profile: null
    };

    this.refresh = this.refresh.bind(this);
    this.updateDatasetView = this.updateDatasetView.bind(this);
    this.printValues = this.printValues.bind(this);
    this.configUpdated = this.configUpdated.bind(this);

  }

  componentWillMount() {
    this.setState({
      profile: getActiveProfile(),
    });
    addConfigObserver(this);
    loadConfiguration();
  }

  configUpdated() {
    this.setState({ profile: getActiveProfile() });
  }
  
  componentDidMount() {
    this.refresh();
  }

  refresh() {
    
    listDatasets((dbdatasets) => {
                   this.setState({
                     datasets: dbdatasets,
                     datasetName1: dbdatasets[0].name,
                     datasetName2: dbdatasets[0].name
                   });
                 }
    );

   if(this.state.views.length <= 0)
   {
    getViews().then((viewIds) => {
      this.setState({
        views: viewIds,
      })
    });
   }
   
  }

  updateDatasetView(event) {
    const [view, name] = event.target.value.split('=:=');
  
   if(view == 'view1'){

	this.setState({
	      datasetName1: name
	    });

    }
    else{

        this.setState({
	      datasetName2: name
	    });

    }   
  }

// ----------------------------------------------------------------------------
// Method to debug this view
// ----------------------------------------------------------------------------

  printValues()
  {
    console.log(this.state.datasetName1 + " " + this.state.datasetName2);
  }

// ----------------------------------------------------------------------------
// It renders the web page. It has two combo boxes each one listing the names of 
// the available datasets. T
// ----------------------------------------------------------------------------

  render() {

    if(this.state.views && this.state.views.length > 0 )
    {
      
      const newTo = { 
        pathname: "/view/dual", 
        views: this.state.views,
        viewNames:  [this.state.datasetName1,this.state.datasetName2],
        profile: this.state.profile,
        history: this.props.history
      }

      return (
        <div className={style.container}>
          <div className={style.content}>
              <select
                   key='select1'
                   value={['view1', this.state.datasetName1].join('=:=')}
                   onChange={this.updateDatasetView}
                   className={this.props.className}
              >
                {this.state.datasets.map((dataset,index) => (
                   <option key={'view1'+index} value={['view1', dataset.name].join('=:=')}>
                            {dataset.name}
                   </option>
                ))}
              </select>

              <select
                  key='select2'
                  value={['view2', this.state.datasetName2].join('=:=')}
                  onChange={this.updateDatasetView}
                  className={this.props.className}
              >
                {this.state.datasets.map((dataset,index) => (
                   <option key={'view2'+index} value={['view2', dataset.name].join('=:=')}>
                           {dataset.name}
                   </option>
                ))}
              </select>

           <Link   
            to={newTo}
            key={'toDualView'}
           >
               <button><p>GO!</p></button> 
           </Link> 
   
     
           <SvgIcon icon={lightVizIcon} width="25px" height="25px" /> 
           <i className={style.refreshButton} onClick={this.refresh} /> 
          </div>
        </div>
      );
    }

    return null;
    
  }
}




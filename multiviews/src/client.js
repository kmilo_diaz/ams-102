// ----------------------------------------------------------------------------
// This file handles the communication between the front and the back end.
// Classes inside the react application have a global pointer to this file, and 
// the methods are invoked using the pointer dot the name of the method
//
// The method does RPC to methods in the back end using two parameters:
// 1. a string with the name of the method in the server side (python in our case)
// 2. the list of parameters received by the method. There is an error if the number 
// or arguments in the call dont match the original number of arguments in the RPC.
// Most of this calls end in the file : multiviews/server/light_viz_protocols.py
//
//
// ----------------------------------------------------------------------------


let connection = null;
let client = null;
let session = null;
let datasets = [];
let activeDataset1 = null;
let activeDataset2 = null;
let structure = [];
let busy = 0;
let lastDS1 = null;
let lastDS2 = null;
const timeCallback = [];
const busyCallBack = [];

let viewsArray = [];

export function setup(conn, clt) {
  connection = conn;
  client = clt;
  session = client.session;

  session.subscribe('pv.time.change', (args) => {
    const timeStep = args[0].timeStep;
    timeCallback.forEach((l) => {
      if (l) {
        l(timeStep);
      }
    });
  });
}

export function getConnection() {
  return connection;
}

export function getClient() {
  return client;
}

export function isBusy() {
  return busy;
}

function triggerBusy(state) {
  for (let i = 0; i < busyCallBack.length; i++) {
    if (busyCallBack[i]) {
      busyCallBack[i](state);
    }
  }
}

function onError(name, error) {
  busy -= 1;
  if (busy === 0) {
    triggerBusy(false);
  }
  console.log(`Error in ${name}`);
}

function onReady() {
  busy -= 1;
  if (busy === 0) {
    triggerBusy(false);
  }
}

function call(method, args) {
  busy += 1;
  if (busy === 1) {
    triggerBusy(true);
  }
  return session.call(method, args);
}

export function onBusyChange(callback) {
  const id = busyCallBack.length;
  busyCallBack.push(callback);
  return id;
}

export function unsubscribeBusy(id) {
  busyCallBack[id] = null;
}


// ----------------------------------------------------------------------------
// Config
// ----------------------------------------------------------------------------

export function getConfiguration(callback) {
  call('light.viz.configuration.get', []).then((output) => {
    onReady();
    callback(output[0], output[1]);
  }, onError.bind(undefined, 'light.viz.configuration.get'));
}

// ----------------------------------------------------------------------------
// Time handler
// In this project we didnt use any of this methods
// ----------------------------------------------------------------------------

export function addTimeListener(callback) {
  const id = timeCallback.length;
  timeCallback.push(callback);
  return id;
}

export function removeTimeListener(id) {
  timeCallback[id] = null;
}

export function playTime(delatT = 0.1) {
  return client.TimeHandler.play(delatT);
}

export function stopTime() {
  return client.TimeHandler.stop();
}

// ----------------------------------------------------------------------------
// ColorMap
// This methods returns the color map of the dataset defined by paraview server
// The front end UI calls this method in order to fill the color map option menu.
// ----------------------------------------------------------------------------

export function listColorMapImages(callback) {
  client.ColorManager.listColorMapImages().then((presets) => {
    callback(presets);
  });
}
// ----------------------------------------------------------------------------
// Dataset
//
// These memthods are called when the front end needs information about the dataset
// Once the user decides to load a dataset, the method loadDataset is called. 
// This call returns a data structure with the information of the dataset included
// the mesh to be renderer and all what the UI options requiere.
// The index parameter in the methods reference the selected view in the front application
// ----------------------------------------------------------------------------

export function  setActiveView(viewId){
    session.call('my.protocols.active.view', [viewId]);
} 


export function getViews ()  {
    return session.call('my.protocols.views', []);
} 


export function addThumbnail(imageElem) {
  const dsName = imageElem.dataset.name;
  call('light.viz.dataset.thumbnail', [dsName]).then(
    (thumbnails) => {
      onReady();
      while (thumbnails.length) {
        const l = thumbnails.length;
        imageElem.dataset[`src-${l}`] = thumbnails.pop();
      }
      const index = parseInt(imageElem.dataset.index, 10) + 1;
      imageElem.src = imageElem.dataset[`src-${index}`];
    },
    (e) => {
      console.log('thumbnails error:', dsName);
    }
  );
}

export function saveThumbnail(viewIndex) {
  call('light.viz.dataset.thumbnail.save', [viewIndex]).then(
    onReady(),
    onError.bind(undefined, 'light.viz.dataset.thumbnail.save')
  );
}

export function queryScreenShot(viewIndex, ssfileName, callback) {
  call('light.viz.dataset.screenshot.save', [viewIndex,ssfileName]).then((filePath) => {
      onReady(),
      callback(filePath);
    },onError.bind(undefined, 'light.viz.dataset.screenshot.save')
  );
}

export function query3DModel(viewIndex, modelfileName, callback) {
  call('light.viz.dataset.model3d.save', [viewIndex,modelfileName]).then((filePath) => {
      onReady(),
      callback(filePath);
    },onError.bind(undefined, 'light.viz.dataset.model3d.save')
  );
}

export function getBlockStructure(viewIndex,callback) {
  call('light.viz.dataset.getblockstructure',[viewIndex]).then((list) => {
    structure = list;
    onReady();
    callback(structure);
  }, onError.bind(undefined, 'light.viz.dataset.getblockstructure'));
}

export function setForegroundColor(color, viewIndex) {

  call('light.viz.foreground.color', [color, viewIndex]).then(
     onReady(),
    onError.bind(undefined, 'light.viz.foreground.color'));

}

export function setBackgroundColor(color, viewIndex, colorFunctionIndex) {
  
   call('light.viz.background'+colorFunctionIndex+'.color', [color, viewIndex]).then(
      onReady(),
      onError.bind(undefined, 'light.viz.background.color')
    );
  
  
}

export function resetCamera(viewId) {
 
  session.call('viewport.camera.reset', [viewId]).then((viewStr)=>{
      console.log("resetCamera 1.5 viewStr: " +viewStr );
    },
    onError.bind(undefined, 'light.viz.contour.values'));
 
}

export function listDatasets(callback) {
  call('light.viz.dataset.list', []).then((list) => {
    datasets = list;
    onReady();
    callback(datasets);
  }, onError.bind(undefined, 'light.viz.dataset.list'));
}

export function updateDatasetOpacity(opacity, viewIndex) {
  session.call('light.viz.dataset.opacity', [opacity, viewIndex]);
}

export function updateBlockVisibility(blockVisibilities, viewIndex) {
 
  session.call('light.viz.dataset.setblock.visibility', [blockVisibilities, viewIndex]);

}

export function loadDataset(viewIndex, dsName, callback) {
 
  if (viewIndex == 0)
  {
    if( activeDataset1 !== dsName || !lastDS1) {
      activeDataset1 = dsName;
      call('light.viz.dataset.load', [viewIndex, activeDataset1]).then((ds) => {
        onReady();
        lastDS1 = ds;
        callback(ds);
      }, onError.bind(undefined, 'light.viz.dataset.load'));
    } else {
      activeDataset1 = dsName;
      callback(lastDS1);
    }
  }
  else
  {
   if( activeDataset2 !== dsName || !lastDS2) {
      activeDataset2 = dsName;
      call('light.viz.dataset.load', [viewIndex, activeDataset2]).then((ds) => {
        onReady();
        lastDS2 = ds;
        callback(ds);
      }, onError.bind(undefined, 'light.viz.dataset.load'));
    } else {
      activeDataset2 = dsName;
      callback(lastDS2);
    }
  }
   
}

export function updateTime(timeIdx) {
  call('light.viz.dataset.time', [timeIdx]).then(
    onReady,
    onError.bind(undefined, 'light.viz.dataset.time')
  );
}

// ----------------------------------------------------------------------------
// Geometry
// When the the UI Tab Geometry is modified, this method is called to modify specific
// info from the dataset
// ----------------------------------------------------------------------------


export function updateGeometryOpacity(opacity, viewIndex) {
  session.call('light.viz.geometry.opacity', [opacity, viewIndex]).then(
    /*onReady(),*/
    onError.bind(undefined, 'light.viz.geometry.opacity')
  );
}

// ----------------------------------------------------------------------------
// Clip
//  When the the UI Tab Clip is created, this method is called to return or modify 
// specific specific info from the dataset
// ----------------------------------------------------------------------------

export function updateClipPosition(viewIndex, { xPosition, yPosition, zPosition }) {
  sesion.call('light.viz.clip.position', [viewIndex, xPosition, yPosition, zPosition]).then(
    /*onReady,*/
    onError.bind(undefined, 'light.viz.clip.position')
  );
}

export function updateClipInsideOut(viewIndex, x, y, z) {
  sesion.call('light.viz.clip.insideout', [viewIndex, x, y, z]).then(
    /*onReady,*/
    onError.bind(undefined, 'light.viz.clip.insideout')
  );
}

export function updateClipBoxPosition(viewIndex, { xPosition, yPosition, zPosition }) {
  sesion.call('light.viz.clip.box.position', [viewIndex, xPosition, yPosition, zPosition]).then(
    /*onReady,*/
    onError.bind(undefined, 'light.viz.clip.box.position')
  );
}

export function enableClipBox(show,viewIndex) {
  sesion.call('light.viz.clip.box.show', [show,viewIndex]).then(
    /*onReady,*/
    onError.bind(undefined, 'light.viz.clip.box.show')
  );
}

// ----------------------------------------------------------------------------
// Contours
//  When the the UI Tab Contours is created, this method is called to return or modify 
// specific specific info from the dataset
// ----------------------------------------------------------------------------

export function updateContourValues(values, viewIndex) {
 
  session.call('light.viz.contour.values', [values, viewIndex]).then(
    onError.bind(undefined, 'light.viz.contour.values')
  );

}

export function updateContourBy(field, viewIndex) {

  session.call('light.viz.contour.by', [field, viewIndex]).then(
    onError.bind(undefined, 'light.viz.contour.by')
  );

}

// ----------------------------------------------------------------------------
// Slices
//  When the the UI Tab Slices is created, this method is called to return or modify 
// specific specific info from the dataset
// ----------------------------------------------------------------------------

export function updateSlicePosition({ xPosition, yPosition, zPosition }) {
  session.call('light.viz.slice.position', [xPosition, yPosition, zPosition]);
}

export function updateSlicesVisible(x, y, z) {
  session.call('light.viz.slice.visibility', [x, y, z]);
}

// ----------------------------------------------------------------------------
// Multi-Slice
//  When the the UI Tab Multi-Slice is created, this method is called to return or modify 
// specific specific info from the dataset
// ----------------------------------------------------------------------------

export function updateMultiSliceValues(positions, viewIndex) {
  session.call('light.viz.mslice.positions', [positions, viewIndex]);
}

export function updateMultiSliceAxis(axis, viewIndex) {
    session.call('light.viz.mslice.normal', [axis, viewIndex]);
}

// ----------------------------------------------------------------------------
// Streamline
//  When the the UI Tab Streamline is created, this method is called to return or modify 
// specific specific info from the dataset
// ----------------------------------------------------------------------------

export function updateStreamlineSeedPoint(viewIndex,{ xPosition, yPosition, zPosition }) {
  session.call('light.viz.streamline.position', [viewIndex, xPosition, yPosition, zPosition]);
}

export function updateStreamlineVector(vector,viewIndex) {
  session.call('light.viz.streamline.vector', [vector, viewIndex]);
}

export function updateStreamlineSeedRadius(radius, viewIndex) {
  session.call('light.viz.streamline.radius', [radius, viewIndex]);
}

export function updateStreamlineNumPoints(numPoints,viewIndex) {
  session.call('light.viz.streamline.numpoints', [numPoints, viewIndex]);
}

export function showStreamlineSeed(enableSeed,viewIndex) {
  session.call('light.viz.streamline.seed.show', [enableSeed, viewIndex]);
}

export function updateStreamlineSeed(position, radius, viewIndex) {
  session.call('light.viz.streamline.seed.update', [position, radius, viewIndex]);
}

// ----------------------------------------------------------------------------
// Threshold
//  When the the UI Tab Threshold is created, this method is called to return or modify 
// specific specific info from the dataset
// ----------------------------------------------------------------------------

export function updateThresholdRange(min, max) {
  call('light.viz.threshold.range', [min, max]).then(
    onReady,
    onError.bind(undefined, 'light.viz.threshold.range')
  );
}

export function updateThresholdBy(name) {
  call('light.viz.threshold.by', [name]).then(
    onReady,
    onError.bind(undefined, 'light.viz.threshold.by')
  );
}
// ----------------------------------------------------------------------------
// color map
//  When the the UI Tab color map is created, this method is called to return or modify 
// specific specific info from the dataset
// ----------------------------------------------------------------------------

export function setColorMapPreset(arrayName, preset, viewIndex) {
  session.call('light.viz.colormap.setpreset', [arrayName, preset,viewIndex]);
}

export function setColorMapRange(arrayName, range,viewIndex) {
  session.call('light.viz.colormap.setrange', [arrayName, range,viewIndex]);
}

export function setColorMapRangeToDataRange(arrayName, callback,viewIndex) {
  session.call('light.viz.colormap.rescale.todatarange', [arrayName,viewIndex]).then((result) => {
    callback(result);
  }, onError.bind(undefined, 'light.viz.colormap.rescale.todatarange'));
}

export function updateOpacityMap(arrayName, controlPoints, viewIndex) {
  session.call('light.viz.opacitymap.set', [arrayName, controlPoints, viewIndex]);
}

export function getColorMap(arrayName, viewIndex, callback) {
  session.call('light.viz.colormap.get',[arrayName, viewIndex]).then((result) => {
   /* onReady(); */
    callback(result.preset, result.range);
  }, onError.bind(undefined, 'light.viz.colormap.get'));
}

export function getOpacityMap(arrayName, callback, viewIndex) {
  session.call('light.viz.opacitymap.get', [arrayName,viewIndex]).then((points) => {
    callback(points);
  }, onError.bind(undefined, 'light.viz.opacitymap.get'));
}

// ----------------------------------------------------------------------------
// Generics
// methods used by all the UI tabs in the front end
// ----------------------------------------------------------------------------

export function updateColor(type, field, location, viewIndex) {

  const name = 'light.viz.TYPE.color'.replace(/TYPE/, type);
  session.call(name, [field, location,viewIndex]);

}

export function enable(type, enabled, viewIndex) {
  const name = 'light.viz.TYPE.enable'.replace(/TYPE/, type);
 
  session.call(name, [enabled, viewIndex]);
}

export function getState(type,viewIndex,...widgets) {
  const name = 'light.viz.TYPE.getstate'.replace(/TYPE/, type);

  call(name,[viewIndex]).then((state) => {
    onReady();
    widgets.forEach((w) => {
      if (w.updateState) {
        w.updateState(state);
      } else {
        w.setState(state);
      }
    });
  }, onError.bind(undefined, name));
}

export function updateUseClip(type, useClip) {
  const name = 'light.viz.TYPE.useclipped'.replace(/TYPE/, type);
  session.call(name, [useClip]);//.then(onReady, onError.bind(undefined, name));
}

// ----------------------------------------------------------------------------

export function updateRepresentation(type, mode, viewIndex) {
 
  const name = 'light.viz.TYPE.representation'.replace(/TYPE/, type);
  session.call(name, [mode, viewIndex]);

}


export function linkCameras() {
         session.call('my.protocols.linkcameras');
}

export function  unLinkCameras(){
         session.call('my.protocols.unlinkcameras');
}

// ----------------------------------------------------------------------------
// This is the way methods form this file are published to the whole
// application.
// ----------------------------------------------------------------------------

export default {
  addThumbnail,
  enable,
  getClient,
  getConnection,
  listDatasets,
  loadDataset,
  resetCamera,
  setup,
  updateClipInsideOut,
  updateClipPosition,
  updateColor,
  updateDatasetOpacity,
  updateRepresentation,
  updateTime,
  onBusyChange,
  unsubscribeBusy,
  getViews,
  setActiveView,
  linkCameras,
  unLinkCameras,
  queryScreenShot,
  query3DModel,
};

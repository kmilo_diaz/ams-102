## ParaViewWeb basic example

The goal of that repository is to provide a basic implementation and usage of ParaViewWeb.

## JavaScript build

```
npm install
npm run build
```

## Running the application

To run the application locally run `pvpython` with the following arguments

```
path_to_praview_bin_folder/pvpython ./server/pvw-light-viz.py --content ./www/ 
```

## Test it in your browser

Open your browser `http://localhost:8080`.

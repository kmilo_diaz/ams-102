# C 2019 John Huffman and Brown University
#
#  Simple python script that uses assimp (external) to generate gltf models
#  from .x3d models, creates a webvr scene for each model based on an existing
#  html template.


import os, sys, subprocess

directory="models/"

# Header for generated html code

print ("""
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
  </head>
  <body>
""")

# get a list of the files in the model directory, sorted backwords by creation date

files = sorted(os.listdir(directory), key=lambda f: os.path.getctime("{}/{}".format(directory, f)), reverse=True)


# For each .x3d file, translate the model to gltf (if it has not been done
#  yet), and create a .html file from a template that has all the aframe
#  stuff already in place.

for filename in files:
  if filename.endswith(".x3d") :

    infile=os.path.join(directory, filename) # Name of .x3d file
    outfile=os.path.join(directory, os.path.splitext(filename)[0]+".gltf")
    htmlfile=os.path.join(directory, os.path.splitext(filename)[0]+".html")
    
    if not os.path.exists(outfile):  # Only generate a 3d model if it doesn't already exist
      os.system("assimp export " + infile +" " +outfile +" >> log.out")
      
    if not os.path.exists(htmlfile):# Only generate HTML file if it doesn't exist
      os.system("sed 's/MODEL/"+os.path.splitext(filename)[0]+".gltf/' template.html > "+htmlfile)
      
    # Print a line in the index.html for each .x3d file found.  
    print("<a href='"+htmlfile+"'><img src='screenshots/"+os.path.splitext(filename)[0]+".jpg' alt='"+os.path.splitext(filename)[0]+"' width=512 height=512></a>")
  
# Footer for generated html code 

print ( """
 </body>
</html>
""")
